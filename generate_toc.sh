#!/usr/local/bin/bash

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <input_html_file>"
  exit 1
fi

input_html="$1"

h2_tags=$(grep '<h2' "$input_html")
h2_tags=$(echo "$h2_tags" | sed '1d')

h3_tags=$(grep '<h3' "$input_html")

#debug
echo "$h2_tags"

toc="<div class=\"toc-wrapper\"><h2>Index</h2><ul>"
while IFS= read -r line; do
  title=$(echo "$line" | sed -n 's/.*>\(.*\)<\/h2>.*/\1/p')
  link=#$(echo "$line" | grep -o 'id="[^"]*"' | sed 's/id="//;s/"//')
  toc+="<li><a href=\"$link\">$title</a></li>"
done <<< "$h2_tags"
toc+="</ul><hr></div>"

head_figure="<figure><div class=\"tg-h-img\"><img src=\"../assets/img/000_freebsd_gearingup_art.png\" alt=\"FreeBSD gearing-up\"></div></figure>"

first_h1=$(grep -m 2 '<\/h1>' "$input_html" | tail -n 1)

echo "$first_h1"

sed -i "\#${first_h1}#a $head_figure" "$input_html"

first_figure=$(grep -m 1 '<\/figure>' "$input_html" | head -n 1)

#debug
echo "$first_figure"

sed -i "\#${first_figure}#a $toc" "$input_html"

echo "Table of Contents has been added and saved to $input_html"
