<h2 align="center">Personal site. Thoughts and guides</h2>
<p align="center">Static site where I place blog posts about using nix systems in content creation workflows</p><br>

<a href="https://www.buymeacoffee.com/n0madcoder"><img src="https://img.shields.io/badge/donate-coffee-orange?&style=flat-square&logo=buy-me-a-coffee"></a>
<a href="https://t.me/s/vertexfarm"><img src="https://img.shields.io/badge/channel-telegram-blue?&style=flat-square&logo=telegram"></a>
