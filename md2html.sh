#!/bin/sh

#variables
output_type="$1"
file_path="$2"

file_name=$(echo ${file_path} | grep -o '[^\/]*$' )

base_name="$( basename "$file_path" | sed 's/.md/.html/' )"

#just type formatted strings
printfn(){
	local msg="${@}"
	printf "%s\n" "${msg}"
}

display_help(){
	printfn "Usage: $ ./md2html -[a|n|bn] path/to/md_file"
	exit
}

main(){
	#check if there are 3 arguments
	# if [ $# -ne 3 ]; then
	# 	printfn "Wrong number of arguments. Expected 3, got $#"
	# 	display_help
	# fi

	case "${output_type}" in
		"-a")
			convert_article
			;;
		"-n")
			convert_note
			;;
		"-bn")
			convert_note_bulk
			;;
		*)
			display_help
			;;
	esac

}

fix_html(){
	printfn "Fixing HTML..."
	# Read the input HTML file
	html_content=$(cat "$base_name")

	# the sed way was working in wsl but it's not on vanilla fbsd
	# modified_content_a=$(echo "$html_content" | sed -e "s/$(echo -e '\\u2019')/$(echo -e '\\u0027')/g")
	fix_content=$(echo "$html_content" | perl -CSD -pe 's/\x{2019}/\x{0027}/g')

	echo "$fix_content" > "$base_name"
}

convert_article(){
	printfn "Converting markdown into ${base_name}..."
	pandoc -f markdown "${file_path}" --wrap=preserve -s --template ./blog-template.html --highlight-style=tango -o "${base_name}"
	fix_html
	mv "${base_name}" content/articles/
	printfn "[Done]"
}

#we can use note-template.html here if we want to differ some styles from articles, but for now we don't
convert_note(){
	printfn "Converting markdown into ${base_name}..."
	pandoc -f markdown "${file_path}" --wrap=preserve -s --template ./blog-template.html --highlight-style=tango -o "${base_name}"
	fix_html
	mv "${base_name}" content/notes/
	printfn "[Done]"
}

convert_note_bulk(){
	bulk=$(find "$file_path" -name "*.md")
	for file in $bulk; do
		file_name=$(echo ${file} | grep -o '[^\/]*$' )
		base_name="$( basename "$file" | sed 's/.md/.html/' )"
		printfn "Converting markdown into ${base_name}..."
		pandoc -f markdown "${file}" --wrap=preserve -s --template ./blog-template.html --highlight-style=tango -o "${base_name}"
		printfn "[Done]"
		printfn "Fixing HTML..."
		html_content=$(cat "$base_name")
		fix_content=$(echo "$html_content" | perl -CSD -pe 's/\x{2019}/\x{0027}/g')
		echo "$fix_content" > "$base_name"
		mv "${base_name}" content/notes/
		printfn "[Done]"
	done
}

main

