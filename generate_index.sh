#!/bin/bash

# if on wsl2 and saved the file from Windows, run sed -i -e 's/\r$//' scriptname.sh to fix line endings

# Hold file name
index="./temp_index.html"

# Create or empty index.html
echo "<ul class=\"list\"></ul>" > $index

# Iterate over markdown files in the directory
for file in ./content/article/*.md; do
	# Extract file name without extension
	filename=$(basename "$file" .md)
	# Convert markdown title to HTML format
	title=$(head -n 1 "$file" | sed 's/# //')
	# Convert creation time to desired format
	creation_time=$(stat -c %y "$file" | awk '{split($0,a," "); print a[1]}')
	# Extract the first paragraph from the markdown file
	first_paragraph=$(sed -n '3 {p;q;}' "$file")
	# Generate the list item element
	list_item="<li><h2><a href=\"./content/articles/${filename}.html\">${title}</a></h2><div class=\"date\"><time>${creation_time}</time></div><div class=\"entry-desc\"><p>${first_paragraph}</p></div></li>"
	# Append the list item element to index.html
	sed -i "/<ul class=\"list\">/a ${list_item}" $index
done
