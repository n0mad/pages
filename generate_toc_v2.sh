#!/usr/local/bin/bash

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <input_html_file>"
  exit 1
fi

input_html="$1"

# Prepare the content to insert
default_img="000_freebsd_gearingup_art.png"
img_src=$(echo "$input_html" | sed 's/\.html$/_art.png/')

# ensure we have an image! if not, use default one
if [ ! -f "../assets/img/$img_src" ]; then
  img_src="$default_img"
fi

head_figure="<figure><div class=\"tg-h-img\"><img src=\"../assets/img/$img_src\" alt=\"standarizing unconformity\"></div></figure>"

# Read the entire HTML content into a variable
html_content=$(cat "$input_html")

# Replace Unicode characters using perl
# html_content=$(echo "$html_content" | perl -CSD -pe 's/\x{2019}/\x{0027}/g')

toc="<div class=\"toc-wrapper\"><h2><a href=\"#\">Index</a></h2><ul class=\"toc_h2\">"

# Variables to keep track of current state
in_h2=false
first_h2_skip=false
current_h2_id=""
current_h2_title=""
h3_list=""

# Read the HTML content line by line
while IFS= read -r line; do
  if echo "$line" | grep -q '<h2'; then
    if [ "$first_h2_skip" = false ]; then
      first_h2_skip=true
      continue
    fi
    if [ "$in_h2" = true ]; then
      if [ -n "$h3_list" ]; then
        toc+="<ul class=\"toc_h3\">$h3_list</ul></li>"
        h3_list=""
      else
        toc+="</li>"
      fi
    fi
    in_h2=true
    current_h2_title=$(echo "$line" | sed -n 's/.*>\(.*\)<\/h2>.*/\1/p')
    current_h2_id=$(echo "$line" | grep -o 'id="[^"]*"' | sed 's/id="//;s/"//')
    toc+="<li><a href=\"#$current_h2_id\">$current_h2_title</a>"
  elif echo "$line" | grep -q '<h3'; then
    if [ "$in_h2" = true ]; then
      h3_title=$(echo "$line" | sed -n 's/.*>\(.*\)<\/h3>.*/\1/p')
      h3_id=$(echo "$line" | grep -o 'id="[^"]*"' | sed 's/id="//;s/"//')
      h3_list+="<li><a href=\"#$h3_id\">$h3_title</a></li>"
    fi
  fi
done <<< "$html_content"

if [ "$in_h2" = true ]; then
  if [ -n "$h3_list" ]; then
    toc+="<ul class=\"toc_h3\">$h3_list</ul></li>"
  else
    toc+="</li>"
  fi
fi

toc+="</ul><hr></div>"

awk -v insert="$head_figure" '
{
  print
  if ($0 ~ /<\/h1>/) {
    count++
    if (count == 2) {
      print insert
    }
  }
}' "$input_html" > temp.html && mv temp.html "$input_html"

awk -v insert="$toc" '
{
  print
  if ($0 ~ /<\/figure>/ && !found) {
    print insert
    found = 1
  }
}' "$input_html" > temp.html && mv temp.html "$input_html"

echo "Table of Contents and figure have been inserted."

