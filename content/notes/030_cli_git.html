<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<html lang="en-US">
	<title>Command line Git | Quick guide</title>

	<meta name="MobileOptimized" content="320">
	<meta name="referrer" content="no-referrer">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="description" content="Feel comfortable using Git from the command line.">
	<meta name="keywords" content="freeBSD, unix, unixworks, c programming, opengl, vulkan, glsl, blender, 3d, substance, workshop, tutorial, linux" />
	<meta property="og:site_name" content="vertexfarm">
	<meta property="og:description" content="In this article guide the goal is to learn how to use Git from the command line. We will go through the different parts of a git based workflow, covering the most used ones.">
	<meta property="og:image" content="https://codeberg.org/n0mad/pages/raw/branch/master/content/assets/img/">
	<meta property="article:published_time" content="">
	<meta name="telegram:channel" content="@vertexfarm">
	<link rel="canonical" href="https://vertexfarm.xyz">
	<link rel="stylesheet" href="../../styles/styles.css">
	<link rel="stylesheet" href="../../styles/tango.css">
	<link href="../../icons/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<!-- Pixel Code for https://overtracking.com/ -->
	<script defer src="https://cdn.overtracking.com/t/t0mTWmkp4QlhdGi4b/"></script>
	<!-- END Pixel Code -->
	<!-- buymeacoffee.com widget -->
	<script data-name="BMC-Widget" data-cfasync="false" src="https://cdnjs.buymeacoffee.com/1.0.0/widget.prod.min.js" data-id="n0madcoder" data-description="Support me on Buy me a coffee!" 
	data-message="" 
	data-color="#9faac6" data-position="Right" data-x_margin="18" data-y_margin="18"></script>
	<!-- END buymeacoffee.com widget -->
	<script src="../../js/clipboard.min.js"></script>
	<script>
		window.onload = function() {
			let pre = document.getElementsByTagName('pre');
	
			for (let i = 0; i < pre.length; i++) {
				let b = document.createElement('button');
				b.className = 'clipboard';
				b.textContent = ' ';
				if (pre[i].childNodes.length === 1 && pre[i].childNodes[0].nodeType === 3) {
					let div = document.createElement('div');
					div.textContent = pre[i].textContent;
					pre[i].textContent = '';
					pre[i].appendChild(div);
				}
				pre[i].appendChild(b);
			}
	
			let clipboard = new ClipboardJS('.clipboard', {
			   target: function(b) {
					let p = b.parentNode;
					if (p.className.includes("highlight")) {
						let elems = p.getElementsByTagName("code");
						if (elems.length > 0)
							return elems[0];
					}
					return p.childNodes[0];
				}
			});
	
			clipboard.on('success', function(e) {
				e.clearSelection();
			});
	
			clipboard.on('error', function(e) {
				console.error('Action:', e.action, e);
				console.error('Trigger:', e.trigger);
			});
		};
	</script>
</head>

<body>
	<div class="nav">
		<h1><a href="../../index.html">vertex farm</a></h1>
		<h2>n0mad coder's blog</h2>
		<ul>
			<li><a href="../000_about.html">About</a></li>
			<li><a href="../notes/000_index.html">Notes</a></li>
			<li><a href="https://codeberg.org/n0mad">Code</a></li>
			<li><a href="https://n0madcoder.itch.io/">Art</a></li>
		</ul>
	</div>

	<div class="content">
<h1 id="command-line-git-quick-guide">Command line Git | Quick guide</h1>
<figure>
	<div class="tg-h-img">
	<img src="../assets/img/000_freebsd_gearingup_art.png" alt="FreeBSD gearing-up">
	</div>
</figure>
<div class="toc-wrapper"><h2>Index</h2><ul><li><a href="#what-is-git">What is Git?</a></li><li><a href="#create-a-repository-from-a-local-directory">Create a repository</a></li><li><a href="#select-what-to-upload">Select what to upload</a></li><li><a href="#commit-and-push-our-content">Commit and Push</a></li><li><a href="#pull-changes-to-our-local-folder">Pull changes</a></li><li><a href="#check-progress-via-commits-and-branches-from-cli">Check progress from cli</a></li><li><a href="#copy-a-repository-from-the-web">Copy a repository</a></li><li><a href="#git-hosting">Git hosting</a></li><li><a href="#summing-up">Summing up</a></li></ul><hr></div>
<p>There are a lot of graphical interfaces to interact with Git with cozy buttons and windows. There's also a fast and powerful alternative: using Git directly from the command line.</p>
<blockquote>
<p>There are different version control systems like Mercurial, Subversion or Bazaar. We are using Git in this guide.</p>
</blockquote>
<h2 id="what-is-git">What is Git?</h2>
<p>Git is a powerful tool to maintain our code projects up to date and keep track of the changes we've made. Using it from the command-line is not that complicated as it could be seen.</p>
<p>To verify that we have <code>git(1)</code> installed in our system, ask for it's version in the command line:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git <span class="at">--version</span></span></code></pre></div>
<p>If the result is similar to <code>git version 0.00.0</code> we are good to go. If not, just grab the package into your system:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> doas pkg install git</span></code></pre></div>
<p>— The first thing to perform after installing git is to set a username and an email address since git commit uses that information every single time.</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git config <span class="at">--global</span> user.name <span class="st">&quot;Your Name&quot;</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git config <span class="at">--global</span> user.email yourname@example.com</span></code></pre></div>
<p>The <code>--global</code> flag is useful if we don't want to write the credentials each time we want to perform an action inside Git, since Git will always use that information for anything you do on that system.</p>
<blockquote>
<p>You can change the default git editor with the <code>git config --global core.editor</code> command.</p>
</blockquote>
<p>In order to override global settings a different name or email address for specific projects, we can run the command without the <code>--global</code> option when working in that project.</p>
<p>— To check our actual settings we only need to ask git for them:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git config <span class="at">--list</span></span></code></pre></div>
<blockquote>
<p>If you want to check where are these variables stored, usually at your <code>$HOME</code> directory you can find a file named <code>.gitconfig</code>.</p>
</blockquote>
<h2 id="create-a-repository-from-a-local-directory">Create a repository from a local directory</h2>
<p>If we have a recently started project that starts growing up and we decide to upload it into a git hosting service we have two options.</p>
<p>— The first one is to create a repository in the hosting service, initialize it with a <code>README.md</code>, clone it in our local drive, and then move all our project inside the cloned repository. This is pretty much self explanatory.</p>
<p>— The second one is to tell git to get our actual project file and upload it into an empty repository hosted in our git service.</p>
<p>The second option is pretty easy to achieve:</p>
<ul>
<li><p>We need to create an empty repository in our git hosting service (github, gitlab, codeberg…) without initializing it.</p></li>
<li><p>Both names (the project directory name and the git repository name) have to match.</p></li>
<li><p>In our local repository we have to initialize git.</p></li>
</ul>
<div class="sourceCode" id="cb5"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git init</span></code></pre></div>
<ul>
<li>After initializing git we have to add the content and commit our action</li>
</ul>
<div class="sourceCode" id="cb6"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git add <span class="at">-A</span></span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git commit <span class="at">-m</span> <span class="st">&quot;commit message&quot;</span></span></code></pre></div>
<ul>
<li>Now we have to remotely add our git origin, which is our newly cloud created repository.</li>
</ul>
<div class="sourceCode" id="cb7"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git remote add origin https://githost.com/username/repository.git</span></code></pre></div>
<ul>
<li>The final step is pushing the content to the origin master branch.</li>
</ul>
<div class="sourceCode" id="cb8"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git push <span class="at">-u</span> origin master</span></code></pre></div>
<p>We'll be asked for our git hosting credentials when pushing content.</p>
<h2 id="select-what-to-upload">Select what to upload</h2>
<p>Chances are that we have come files inside our local project that we don't want to upload, like temporary files that the system creates or test builds that serve for debugging purposes.</p>
<p>— We can create a special file for git that allow us to specify which content to omit when pushing the project to the git service.</p>
<p>This file needs to be named <code>.gitignore</code> and it's a good idea to create it in the top level directory of our repository.</p>
<p>Git uses <em>globbing</em> patterns to match against file names. We can construct our patterns using a set of symbols:</p>
<ul>
<li><code>**</code> A pattern with a double asterisk will match directories anywhere in the repository.</li>
</ul>
<div class="sourceCode" id="cb9"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a><span class="ex">**/debug</span></span></code></pre></div>
<ul>
<li><code>*.</code> A patter with an asterisk will match zero or more characters anywhere in the repository.</li>
</ul>
<div class="sourceCode" id="cb10"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true" tabindex="-1"></a><span class="ex">*.o</span> <span class="pp">*</span>.log</span></code></pre></div>
<ul>
<li><code>!</code> Prepending an exclamation mark to a pattern negates it. A file will not be ignored if it matches a pattern, but also matches a negating pattern defined later in the file.</li>
</ul>
<div class="sourceCode" id="cb11"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb11-1"><a href="#cb11-1" aria-hidden="true" tabindex="-1"></a><span class="ex">!important.log</span></span></code></pre></div>
<h2 id="commit-and-push-our-content">Commit and Push our content</h2>
<p>Once we've made changes locally to our code or project, we need to merge them into the hosted git repository.</p>
<p>First we need to tell git to add our changes. We can add all the files with the flag <code>-A</code> or all the modified files with the flag <code>.</code></p>
<div class="sourceCode" id="cb12"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb12-1"><a href="#cb12-1" aria-hidden="true" tabindex="-1"></a><span class="co"># add all files in the repository</span></span>
<span id="cb12-2"><a href="#cb12-2" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git add <span class="at">-A</span></span>
<span id="cb12-3"><a href="#cb12-3" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb12-4"><a href="#cb12-4" aria-hidden="true" tabindex="-1"></a><span class="co"># add all modified files in the repository</span></span>
<span id="cb12-5"><a href="#cb12-5" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git add .</span></code></pre></div>
<p>The following step is to commit the changes, usually with a comment.</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb13-1"><a href="#cb13-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git commit</span></code></pre></div>
<p>If we want to make a one line comment we can add the flag -m after commit and write inside double quotes our message:</p>
<div class="sourceCode" id="cb14"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb14-1"><a href="#cb14-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git commit <span class="at">-m</span> <span class="st">&quot;Updated foo.c -Changed boo function -Removed trash&quot;</span></span></code></pre></div>
<p>In the short run, we are most likely going to remember what we did in that commit. A lot of commit messages are similar to “update code” or “wrote function boo”.</p>
<p>In the long run, you're going to love the time spent writing the commit messages with detail and common sense. Here's a short template of how can we structure a commit message:</p>
<div class="sourceCode" id="cb15"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb15-1"><a href="#cb15-1" aria-hidden="true" tabindex="-1"></a><span class="ex">Summarize</span> the change in a few but meaningful words.</span>
<span id="cb15-2"><a href="#cb15-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb15-3"><a href="#cb15-3" aria-hidden="true" tabindex="-1"></a><span class="ex">Additions:</span></span>
<span id="cb15-4"><a href="#cb15-4" aria-hidden="true" tabindex="-1"></a><span class="ex">-</span> what you added</span>
<span id="cb15-5"><a href="#cb15-5" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb15-6"><a href="#cb15-6" aria-hidden="true" tabindex="-1"></a><span class="ex">Fixes:</span></span>
<span id="cb15-7"><a href="#cb15-7" aria-hidden="true" tabindex="-1"></a><span class="ex">-</span> what it fixes</span>
<span id="cb15-8"><a href="#cb15-8" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb15-9"><a href="#cb15-9" aria-hidden="true" tabindex="-1"></a><span class="ex">Changes:</span></span>
<span id="cb15-10"><a href="#cb15-10" aria-hidden="true" tabindex="-1"></a><span class="ex">-</span> what it changes</span></code></pre></div>
<p>Longer explanation if needed goes here, along with additional notes, or relevant info.</p>
<p>When typing git commit without the -m flag, the shell will open the default text editor and will ask us to write the commit message.</p>
<h2 id="pull-changes-to-our-local-folder">Pull changes to our local folder</h2>
<p>Every time we access the repository locally, we need to keep track of the cloud updates, so the work can flow seamlessly.</p>
<p>The first thing we have to do before start working inside the local repository, is to check for changes:</p>
<div class="sourceCode" id="cb16"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb16-1"><a href="#cb16-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git status</span></code></pre></div>
<p>if we have changes we can add them to our local repository via git pull:</p>
<div class="sourceCode" id="cb17"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb17-1"><a href="#cb17-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git pull</span></code></pre></div>
<p>Then we can start messing around.</p>
<p>— Things went nuts, the content inside the cloud repository had new changes but we were working locally without pulling them first!</p>
<p>Don't worry, there's a solution for that. You can stash (hide) your changes, pull and then apply your changes again:</p>
<div class="sourceCode" id="cb18"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb18-1"><a href="#cb18-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git stash <span class="at">-u</span></span>
<span id="cb18-2"><a href="#cb18-2" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git pull</span>
<span id="cb18-3"><a href="#cb18-3" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git stash pop </span></code></pre></div>
<h2 id="check-progress-via-commits-and-branches-from-cli">Check progress via commits and branches from cli</h2>
<p>GUI tools make pretty graphics to showcase a git project. Guess what, you can do the same with the command-line using <code>git log</code>:</p>
<div class="sourceCode" id="cb19"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb19-1"><a href="#cb19-1" aria-hidden="true" tabindex="-1"></a><span class="fu">git</span> log <span class="at">--graph</span> <span class="at">--format</span><span class="op">=</span>format:<span class="st">&#39;%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%an%C(reset)%C(bold yellow)%d%C(reset) %C(dim white)- %s%C(reset)&#39;</span> <span class="at">--all</span></span></code></pre></div>
<ul>
<li><p>The <code>--all</code> flag is used to show all commits.</p></li>
<li><p>The <code>--graph</code> flag is used to show the commit graph, that is, the cozy lines that represent the commit paths and progress.</p></li>
<li><p>The <code>--format</code> flag is used to customize the output. In this case we show all commits in a graph with a colored commit message, author, date and the commit hash in the output.</p></li>
</ul>
<h2 id="copy-a-repository-from-the-web">Copy a repository from the web</h2>
<p>This is probably something you already know, but just for refreshing the memory, let's take a brief look at it.</p>
<p>When we want to get a repository from the web, we have an option to zip the entire repository and download it with just one click in the specified icon. This requires to manually unzip it later. But we are trying to use git from the command-line, and extra steps like unzip projects aren't part of the goal.</p>
<p>Every code repository has an <code>https</code> direction we can use to clone using git in the command-line in a very easy and quick way:</p>
<ul>
<li><p>First we need to create (or navigate to) a directory where we would like to store the git project.</p></li>
<li><p>The second step is to copy that url and clone it via git clone:</p></li>
</ul>
<div class="sourceCode" id="cb20"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb20-1"><a href="#cb20-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git clone https://hosting.site/user/repo-name.git</span></code></pre></div>
<h2 id="git-hosting">Git hosting</h2>
<p>Project tracking in <code>git</code> is great, but we need to keep our repository somewhere. One of the solutions is to create our own server and make our own repository with or without tools like Gogs.</p>
<p>To create a local git repository we need to have a <code>git</code> user account with some permissions in the machine where we would like to store our project.</p>
<blockquote>
<p>From <a href="https://git-scm.com/book/en/v2/Git-on-the-Server-Setting-Up-the-Server">the official Git documentation</a>:</p>
</blockquote>
<div class="sourceCode" id="cb21"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb21-1"><a href="#cb21-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> doas adduser git</span>
<span id="cb21-2"><a href="#cb21-2" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> su git</span>
<span id="cb21-3"><a href="#cb21-3" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> cd</span>
<span id="cb21-4"><a href="#cb21-4" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> mkdir .ssh <span class="kw">&amp;&amp;</span> <span class="fu">chmod</span> 700 .ssh</span>
<span id="cb21-5"><a href="#cb21-5" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> touch .ssh/authorized_keys <span class="kw">&amp;&amp;</span> <span class="fu">chmod</span> 600 .ssh/authorized_keys</span></code></pre></div>
<blockquote>
<p>Remember to add your public key to the authorized_keys file. <code>$ cat /tmp/id_rsa.yourusername.pub &gt;&gt; ~/.ssh/authorized_keys</code></p>
</blockquote>
<p>Once done, under the <code>git</code> user <code>home</code> directory we can create a subdirectory where we would like to store our git projects, and initialize new ones with the following command:</p>
<div class="sourceCode" id="cb22"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb22-1"><a href="#cb22-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> mkdir /home/git/repositories</span>
<span id="cb22-2"><a href="#cb22-2" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git init <span class="at">--bare</span> <span class="at">--shared</span><span class="op">=</span>group /home/git/repositories/project-name.git</span></code></pre></div>
<p>Now from your machine you can do as follows in order to initialize the repo and start working (assuming that we are in an existing project with files in it):</p>
<div class="sourceCode" id="cb23"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb23-1"><a href="#cb23-1" aria-hidden="true" tabindex="-1"></a><span class="co"># your machine</span></span>
<span id="cb23-2"><a href="#cb23-2" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> cd project-name</span>
<span id="cb23-3"><a href="#cb23-3" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git init</span>
<span id="cb23-4"><a href="#cb23-4" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git add .</span>
<span id="cb23-5"><a href="#cb23-5" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git commit <span class="at">-m</span> <span class="st">&#39;Initial commit&#39;</span></span>
<span id="cb23-6"><a href="#cb23-6" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git remote add origin git@gitserverip:/srv/git/project-name.git</span>
<span id="cb23-7"><a href="#cb23-7" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git push origin master</span></code></pre></div>
<p>At this point you can let the rest of the team use the git repository as usual.</p>
<div class="sourceCode" id="cb24"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb24-1"><a href="#cb24-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git clone git@gitserver:/srv/git/project.git</span>
<span id="cb24-2"><a href="#cb24-2" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> cd project</span>
<span id="cb24-3"><a href="#cb24-3" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> vim README</span>
<span id="cb24-4"><a href="#cb24-4" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git commit <span class="at">-am</span> <span class="st">&#39;Fix for README file&#39;</span></span>
<span id="cb24-5"><a href="#cb24-5" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> git push origin master</span></code></pre></div>
<blockquote>
<p>The workflow examples have been extracted from the official Git documentation. Refer to that for further and deeper explanations on how to use Git.</p>
</blockquote>
<p>— Another way is to go online and register in a git hosting service. The most popular out there is GitHub. Since Microsoft acquired it, is becoming a social hub where developers share code, follow each others, post updates and sponsor projects they like. There's nothing wrong going GitHub. Just be sure to read the terms &amp; conditions carefully if you're concern about privacy, and don't forget to license your code.</p>
<p>Luckily there are alternatives to GitHub. All the following services provide options to store your code freely and the ability to decide whether your code is public or private.</p>
<ul>
<li>Gitea is based on Gogs. It's offered as a self-hosted service but you can use an already free hosted service at gitea.com</li>
<li>Codeberg is great to store open source projects. It's based on Gitea.</li>
<li>NotABug is based on Gogs and offers free code hosting for any project that is distributed under any free license.</li>
<li>Gitlab is a commercial git service that offers enterprise ready tools and also a free plan where you can store your code and take advantage of a limited set of their tools.</li>
<li>Bitbucket is another commercial git service aimed for teams and big projects. If you work solo or your project is less than five persons, you can opt for a free plan.</li>
</ul>
<h2 id="summing-up">Summing up</h2>
<p>Git is way more complex and powerful than just the content we read here. There's no point in copy-pasting complex workflows and custom needs in a guide that pretends to explain core common things and be useful in a hurry .</p>
<p>If you want to deep dive in Git, there is an official book available to read for free online in the official Git site.</p>
	</div>
	<footer>
		<div class="footer-grid">
			<div class="footer-info">
			<p>2025 - n0madcoder<br>
			<a href="mailto:nomadcoder@vertexfarm.xyz">nomadcoder@vertexfarm.xyz</a></p>
			</div>
			<div class="footer-social">
				<a href="https://t.me/s/vertexfarm">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm-3.11-8.83l.013-.007.87 2.87c.112.311.266.367.453.341.188-.025.287-.126.41-.244l1.188-1.148 2.55 1.888c.466.257.801.124.917-.432l1.657-7.822c.183-.728-.137-1.02-.702-.788l-9.733 3.76c-.664.266-.66.638-.12.803l2.497.78z"/></svg>
				</a>
				<a href="https://buymeacoffee.com/n0madcoder">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M5 3h15a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-2v3a4 4 0 0 1-4 4H8a4 4 0 0 1-4-4V4a1 1 0 0 1 1-1zm13 2v3h2V5h-2zM2 19h18v2H2v-2z"/></svg>
				</a>
			</div>
		</div>
	</footer>
</body>

</html>
