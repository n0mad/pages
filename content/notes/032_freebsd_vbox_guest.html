<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<html lang="en-US">
	<title>FreeBSD in VirtualBox as a Guest</title>

	<meta name="MobileOptimized" content="320">
	<meta name="referrer" content="no-referrer">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="description" content="Configure a Desktop Workspace for FreeBSD inside VirtualBox as a guest.">
	<meta name="keywords" content="freeBSD, unix, unixworks, c programming, opengl, vulkan, glsl, blender, 3d, substance, workshop, tutorial, linux" />
	<meta property="og:site_name" content="vertexfarm">
	<meta property="og:description" content="An ideal world would allow all of us to use our most beloved operating system as the one and only daily driver for work, hobby, gaming and anything in between. But let's say we don't have that ideal world yet. So what do we do? Virtualisation.">
	<meta property="og:image" content="https://codeberg.org/n0mad/pages/raw/branch/master/content/assets/img/">
	<meta property="article:published_time" content="2023-11-04">
	<meta name="telegram:channel" content="@vertexfarm">
	<link rel="canonical" href="https://vertexfarm.xyz">
	<link rel="stylesheet" href="../../styles/styles.css">
	<link rel="stylesheet" href="../../styles/tango.css">
	<link href="../../icons/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<!-- Pixel Code for https://overtracking.com/ -->
	<script defer src="https://cdn.overtracking.com/t/t0mTWmkp4QlhdGi4b/"></script>
	<!-- END Pixel Code -->
	<!-- buymeacoffee.com widget -->
	<script data-name="BMC-Widget" data-cfasync="false" src="https://cdnjs.buymeacoffee.com/1.0.0/widget.prod.min.js" data-id="n0madcoder" data-description="Support me on Buy me a coffee!" 
	data-message="" 
	data-color="#9faac6" data-position="Right" data-x_margin="18" data-y_margin="18"></script>
	<!-- END buymeacoffee.com widget -->
	<script src="../../js/clipboard.min.js"></script>
	<script>
		window.onload = function() {
			let pre = document.getElementsByTagName('pre');
	
			for (let i = 0; i < pre.length; i++) {
				let b = document.createElement('button');
				b.className = 'clipboard';
				b.textContent = ' ';
				if (pre[i].childNodes.length === 1 && pre[i].childNodes[0].nodeType === 3) {
					let div = document.createElement('div');
					div.textContent = pre[i].textContent;
					pre[i].textContent = '';
					pre[i].appendChild(div);
				}
				pre[i].appendChild(b);
			}
	
			let clipboard = new ClipboardJS('.clipboard', {
			   target: function(b) {
					let p = b.parentNode;
					if (p.className.includes("highlight")) {
						let elems = p.getElementsByTagName("code");
						if (elems.length > 0)
							return elems[0];
					}
					return p.childNodes[0];
				}
			});
	
			clipboard.on('success', function(e) {
				e.clearSelection();
			});
	
			clipboard.on('error', function(e) {
				console.error('Action:', e.action, e);
				console.error('Trigger:', e.trigger);
			});
		};
	</script>
</head>

<body>
	<div class="nav">
		<h1><a href="../../index.html">vertex farm</a></h1>
		<h2>n0mad coder's blog</h2>
		<ul>
			<li><a href="../000_about.html">About</a></li>
			<li><a href="../notes/000_index.html">Notes</a></li>
			<li><a href="https://codeberg.org/n0mad">Code</a></li>
			<li><a href="https://n0madcoder.itch.io/">Art</a></li>
		</ul>
	</div>

	<div class="content">
<h1 id="freebsd-in-virtualbox-as-a-guest">FreeBSD in VirtualBox as a Guest</h1>
<figure>
	<div class="tg-h-img">
	<img src="../assets/img/000_freebsd_gearingup_art.png" alt="FreeBSD gearing-up">
	</div>
</figure>
<div class="toc-wrapper"><h2>Index</h2><ul><li><a href="#virtualisation-to-the-rescue">Virtualisation to the rescue</a></li></ul><hr></div>
<p id="sub-desc">
An ideal world would allow all of us to use our most beloved operating system as the one and only daily driver for work, hobby, gaming and anything in between.
</p>
<p>— But let's say we don't have that ideal world yet. So what do we do? Virtualisation.</p>
<blockquote>
<p>There is an extensive guide on FreeBSD virtualisation on the <a href="https://docs.freebsd.org/en/books/handbook/virtualization/">Handbook</a>. This guide aims to be a savvy setup only. Further reference should be taken from the Handbook entry.</p>
</blockquote>
<h2 id="virtualisation-to-the-rescue">Virtualisation to the rescue</h2>
<p id="sub-desc">
FreeBSD can be virtualised using <a href="https://www.virtualbox.org/">VirtualBox</a>. Just download an <code>ISO</code> image from the <a href="https://download.freebsd.org/releases/amd64/amd64/ISO-IMAGES/13.2/">official site</a> and install it.
</p>
<blockquote>
<p>For this example, the host is a Windows 10 x64 machine.</p>
</blockquote>
<p>FreeBSD should work out of the box on the fresh install inside VirtualBox however, in order to enhance the experience, we need to tweak it a bit.</p>
<p>Since we are using the OS as a guest, we should only need to install the following package:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> doas pkg install <span class="at">-y</span> virtualbox-ose-additions</span></code></pre></div>
<p><code>/etc/rc.conf</code>:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="va">vboxguest_enable</span><span class="op">=</span><span class="st">&quot;YES&quot;</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a><span class="va">vboxservice_enable</span><span class="op">=</span><span class="st">&quot;YES&quot;</span></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a><span class="va">vboxservice_flags</span><span class="op">=</span><span class="st">&quot;--disable-timesync&quot;</span></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a><span class="va">vboxvfs_load</span><span class="op">=</span><span class="st">&quot;YES&quot;</span></span></code></pre></div>
<p>You will notice that the moment you restart the vm, only the borderless mouse interaction works out of the box. You can enable the rest of features using the cli tool <code>VBoxClient</code>. The flag <code>--help</code> will tell you all the available options.</p>
<p>Some really cool feature is the bidirectional clipboard. You can enable it by typing <code>VBoxClient --clipboard</code>. If everything goes fine you should be able to perform copy-paste operations from and to both the host and guest.</p>
<blockquote>
<p>Features enabled via VBoxClient are not automatically enabled at boot time, you may need to create a custom script that enables the one you want after executing <code>.xinitrc</code>.</p>
</blockquote>
<h3 id="fixing-the-tty-size">Fixing the tty size</h3>
<p id="sub-desc">
Once you boot FreeBSD from VirtualBox, if your screen has a big resolution you'll notice that the buffer offered by default from VirtualBox is too small.
</p>
<p>This happens due to the new virtual terminal implemented in FreeBSD, <code>Newcons</code> or <code>vt</code>. <code>vt</code> is in active development and as for now it has limited support for modesetting. It adds the following features over <code>sc</code>:</p>
<ul>
<li>Unicode support.</li>
<li>Double-width character support for CJK characters.</li>
<li>Bigger font maps (because of the use of graphics mode).</li>
<li>Better use of graphics modes, enabling more appealing graphical boot experiences.</li>
</ul>
<blockquote>
<p>It's true that when using a KMS driver like <code>i915kms</code> or <code>radeonkms</code>, you can set a mode in <code>/boot/loader.conf</code> like <code>kern.vt.fb.default_mode="1024x768"</code>.</p>
</blockquote>
<p>Since we are using a virtual machine with no intention on accelerated graphics (at least for now), the fastest solution to our limited tty size is to change the virtual console from <code>vt</code> to <code>sc</code>.</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> doas echo <span class="st">&#39;kern.vty=sc&#39;</span> <span class="op">&gt;&gt;</span> /boot/loader.conf</span></code></pre></div>
<p>Once the setting is changed, we can check the available modes for the <code>sc</code> tty using <code>vidcontrol</code>:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> vidcontrol <span class="at">-i</span> mode</span></code></pre></div>
<blockquote>
<p>This workaround is confirmed to be working in 13.2-RELEASE. A screen of <code>1024x768x24</code> should be MODE_280</p>
</blockquote>
<p>To make your selection permanent, you will have to add it to <code>/etc/rc.conf</code>: <code>allscreens_flags="MODE_280"</code>.</p>
<h3 id="adding-custom-resolutions-for-x11">Adding custom resolution(s) for X11</h3>
<p id="sub-desc">
If you plan to use the virtual machine with X11, there are a couple steps you can do to improve the screen resolution once you are in the window manager of choice.
</p>
<p>First, from the VirtualBox UI under the Settings menu, select the <em>Display</em> tab. You should see there a dropdown menu for the Graphics Controller. Despite a yellow warning discouraging you to use <code>VBoxVGA</code> GC, select it.</p>
<p>Disable the option for Enabling 3D Acceleration if it's enabled.</p>
<p>Then from the OS guest, we can use <code>xrandr(1)</code> to set the resolution. If you don't find the desired resolution when executing the tool without any args, then you can try to setup a custom one.</p>
<p>In this guide, we are missing the <code>1366x768</code> resolution in the guest OS. We can add it by first creating a new <em>modeline</em> with:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> cvt 1366 768 60</span></code></pre></div>
<p>where the <code>60</code> is the refresh rate in Hz.</p>
<p>It should generate a line that looks like this:</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="ex">Modeline</span> <span class="st">&quot;1366x768_60.00&quot;</span>  85.25  1366 1440 1576 1784  768 771 781 798 <span class="at">-hsync</span> +vsync</span></code></pre></div>
<p>Now, using <code>xrandr(1)</code> we can create the new mode, and add it:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> xrandr <span class="at">--newmode</span> <span class="st">&quot;1366x768_60.00&quot;</span>  85.25  1366 1440 1576 1784  768 771 781 798 <span class="at">-hsync</span> +vsync</span>
<span id="cb7-2"><a href="#cb7-2" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> xrandr <span class="at">--addmode</span> <span class="st">&quot;1366x768_60.00&quot;</span></span></code></pre></div>
<p>If you run <code>xrandr(1)</code> again, you should see the new mode available in the list. To make the changes permanent, add the previous two lines into <code>~/.xprofile</code>.</p>
<p>As a last step, we can enable the new mode from <code>.xinitrc</code>: <code>xrandr -s 1366x768_60.00</code> so each time we launch the graphic environment, the resolution is automatically set.</p>
<h3 id="enabling-shared-directories">Enabling shared directories</h3>
<p id="sub-desc">
Using a virtual machine to work often needs that the user can share data between the host and the guest. The most common way to do so is through ssh, but we can also use shared directories from within VirtualBox.
</p>
<p>In order to achieve it this way, we need to follow these steps:</p>
<ul>
<li>Create a directory in your host that you want to share with the guest.</li>
<li>Create a directory in your VM that will be used as a mount point.</li>
<li>Add it to the VirtualBox guest properties from within the Settings UI.</li>
<li>Enable <code>Shared Folders</code> in the VirtualBox Settings UI.</li>
</ul>
<p>From the FreeBSD guest side, we can mount the shared directory as:</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> doas mount <span class="at">-t</span> vboxvfs <span class="at">-o</span> rw,gid=1001,uid=1001 vmshare /mnt/vmshare</span></code></pre></div>
<p>where the <code>gid</code> and <code>uid</code> are the owner of the directory and can be obtained by running <code>$ id &lt;username&gt;</code>.</p>
<p>In order to automatically mount the shared directory on boot, we need to add the following line to <code>/etc/fstab</code>:</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a><span class="ex">vmshare</span> /mnt/vmshare vboxvfs rw,gid=1001,uid=1001 0 0</span></code></pre></div>
<p>And from the VirtualBox host side, we have to check-enable the <code>Auto-mount</code> option at the <em>Settings</em> tab <code>Shared Folders -&gt; your_shared_dir -&gt; Edit</code>.</p>
<p>From now on, each time you boot your FreeBSD virtual machine, it will automatically mount the shared directory and you will have access to it.</p>
	</div>
	<footer>
		<div class="footer-grid">
			<div class="footer-info">
			<p>2025 - n0madcoder<br>
			<a href="mailto:nomadcoder@vertexfarm.xyz">nomadcoder@vertexfarm.xyz</a></p>
			</div>
			<div class="footer-social">
				<a href="https://t.me/s/vertexfarm">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm-3.11-8.83l.013-.007.87 2.87c.112.311.266.367.453.341.188-.025.287-.126.41-.244l1.188-1.148 2.55 1.888c.466.257.801.124.917-.432l1.657-7.822c.183-.728-.137-1.02-.702-.788l-9.733 3.76c-.664.266-.66.638-.12.803l2.497.78z"/></svg>
				</a>
				<a href="https://buymeacoffee.com/n0madcoder">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M5 3h15a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-2v3a4 4 0 0 1-4 4H8a4 4 0 0 1-4-4V4a1 1 0 0 1 1-1zm13 2v3h2V5h-2zM2 19h18v2H2v-2z"/></svg>
				</a>
			</div>
		</div>
	</footer>
</body>

</html>
