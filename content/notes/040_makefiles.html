<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<html lang="en-US">
	<title>Makefiles | The power to build</title>

	<meta name="MobileOptimized" content="320">
	<meta name="referrer" content="no-referrer">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="description" content="Learn how to use Makefiles to build C/C++ programs.">
	<meta name="keywords" content="freeBSD, unix, unixworks, c programming, opengl, vulkan, glsl, blender, 3d, substance, workshop, tutorial, linux" />
	<meta property="og:site_name" content="vertexfarm">
	<meta property="og:description" content="Working with languages like C/C++ requires running a process to compile our project. That process can look like a black box where magic happens, but it's not that complicated. Let's see how Makefiles work and how to write practical ones for your everyday hacks and projects.">
	<meta property="og:image" content="https://codeberg.org/n0mad/pages/raw/branch/master/content/assets/img/000_freebsd_gearingup_art.png">
	<meta property="article:published_time" content="2020-01-06">
	<meta name="telegram:channel" content="@vertexfarm">
	<link rel="canonical" href="https://vertexfarm.xyz">
	<link rel="stylesheet" href="../../styles/styles.css">
	<link rel="stylesheet" href="../../styles/tango.css">
	<link href="../../icons/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<!-- Pixel Code for https://overtracking.com/ -->
	<script defer src="https://cdn.overtracking.com/t/t0mTWmkp4QlhdGi4b/"></script>
	<!-- END Pixel Code -->
	<!-- buymeacoffee.com widget -->
	<script data-name="BMC-Widget" data-cfasync="false" src="https://cdnjs.buymeacoffee.com/1.0.0/widget.prod.min.js" data-id="n0madcoder" data-description="Support me on Buy me a coffee!" 
	data-message="" 
	data-color="#9faac6" data-position="Right" data-x_margin="18" data-y_margin="18"></script>
	<!-- END buymeacoffee.com widget -->
	<script src="../../js/clipboard.min.js"></script>
	<script>
		window.onload = function() {
			let pre = document.getElementsByTagName('pre');
	
			for (let i = 0; i < pre.length; i++) {
				let b = document.createElement('button');
				b.className = 'clipboard';
				b.textContent = ' ';
				if (pre[i].childNodes.length === 1 && pre[i].childNodes[0].nodeType === 3) {
					let div = document.createElement('div');
					div.textContent = pre[i].textContent;
					pre[i].textContent = '';
					pre[i].appendChild(div);
				}
				pre[i].appendChild(b);
			}
	
			let clipboard = new ClipboardJS('.clipboard', {
			   target: function(b) {
					let p = b.parentNode;
					if (p.className.includes("highlight")) {
						let elems = p.getElementsByTagName("code");
						if (elems.length > 0)
							return elems[0];
					}
					return p.childNodes[0];
				}
			});
	
			clipboard.on('success', function(e) {
				e.clearSelection();
			});
	
			clipboard.on('error', function(e) {
				console.error('Action:', e.action, e);
				console.error('Trigger:', e.trigger);
			});
		};
	</script>
</head>

<body>
	<div class="nav">
		<h1><a href="../../index.html">vertex farm</a></h1>
		<h2>n0mad coder's blog</h2>
		<ul>
			<li><a href="../000_about.html">About</a></li>
			<li><a href="../notes/000_index.html">Notes</a></li>
			<li><a href="https://codeberg.org/n0mad">Code</a></li>
			<li><a href="https://n0madcoder.itch.io/">Art</a></li>
		</ul>
	</div>

	<div class="content">
<h1 id="makefiles-the-power-to-build">Makefiles | The power to build</h1>
<figure><div class="tg-h-img"><img src="../assets/img/000_freebsd_gearingup_art.png" alt="FreeBSD gearing-up"></div></figure>
<div class="toc-wrapper"><h2>Index</h2><ul><li><a href="#flags">Flags</a></li><li><a href="#basic-structure">Basic structure</a></li><li><a href="#summing-up">Summing up</a></li></ul><hr></div>
<p>Working with languages like C/C++ requires running a process to compile our project. That process can look like a black box where magic happens, but it's not that complicated. Let's see how Makefiles work and how to write practical ones for your everyday hacks and projects.</p>
<blockquote>
<p>You've maybe heard of <code>Makefile</code> generators like <code>cmake</code>. We're not using them here, neither a heavy IDE. The article is explained using a plain text editor and the command-line.</p>
</blockquote>
<p><code>make</code> is a tool used mostly to compile and build C/C++ source code. Makefiles just tell <code>make</code> how to compile and build a program. They consist in a series of instructions that perform automatically rather than having to manually type them in the command-line.</p>
<p>— Before we go further with <code>make</code>, let's check what happens when we call the compiler so we know how to structure steps inside the Makefile later.</p>
<p>A simple compiling process takes four steps:</p>
<ul>
<li>The first step a compiler does is take our <code>.c</code> files and call the preprocessor, which handle the directives that start with a <code>#</code> like <code>#include</code> and <code>#define</code> and gets rid of the comments that may be present in our code.</li>
</ul>
<p><em>At this point, all the code inside the header files that we have included using the directive <code>#include "header.h"</code> is copied and pasted in the program.</em></p>
<ul>
<li>The second step takes the source file and calls the compiler to translate C code into Assembly code, ending up with a file that has a <code>.s</code> extension.</li>
<li>Once the compiler is done, it needs to translate the Assembly code into machine code, creating an object file, which is done via the assembler. The result file <code>.o</code> isn't an executable yet.</li>
<li>The last step is bringing together all the object files to produce an executable. This part is done with the linker.</li>
</ul>
<h2 id="flags">Flags</h2>
<p>Each one of the steps needed to build a program can be invoked with a specific option to the compiler.</p>
<p>Flags give us the ability to enable or disable functionality for our building processes.</p>
<ul>
<li><code>-E</code> calls the preprocessor only.</li>
<li><code>-S</code> runs the compiler and stops at the assembly file.</li>
<li><code>-c</code> is used to run the compiler up to the creation of an object file.</li>
<li><code>-o</code> generates the executable program from the object files.</li>
<li><code>-g</code> allows using gdb debugging.</li>
<li><code>-Wall</code> enables the compiler to print all warnings encountered while building the program.</li>
<li><code>-I</code> specifies a directory that contains prerequisites.</li>
</ul>
<p>All the previous flags can be manually typed in a command-line environment:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> cc <span class="at">-o</span> demo_program main.c</span></code></pre></div>
<p>but the idea here is to store those commands in a <code>Makefile</code> to automatically perform the build.</p>
<h2 id="basic-structure">Basic structure</h2>
<p>A Makefile (case sensitivity named) is a plain text file that can contain the following sections:</p>
<ul>
<li>Rules (that can be explicit or implicit)</li>
<li>Macros (variable definitions)</li>
<li>Comments</li>
</ul>
<h3 id="rules">Rules</h3>
<p>A Makefile rule needs three basic items:</p>
<ul>
<li>A target, which is the name of the generated file.</li>
<li>The dependencies needed to build the target.</li>
<li>An action to realize in order to get the target.</li>
</ul>
<p>Actions need to be indented by a tab character (not spaces) in order to work.</p>
<blockquote>
<p>Note that we can have more than one action per target, and each one needs to be in a new line.</p>
</blockquote>
<div class="sourceCode" id="cb2"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a><span class="dv">target:</span><span class="dt"> dependencies</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a>    action</span></code></pre></div>
<p>Let's pretend that we have a set of source files that we can compile into a program named calculator which depends on five independent source files.</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a><span class="dv">calculator :</span><span class="dt"> main.c sum.h sub.h mult.h div.h</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a>    cc -o calculator main.c</span></code></pre></div>
<ul>
<li>Our target can be named as the program we want to create, so in this case is calculator.</li>
<li>Our dependencies are five object files. Each of those files comes from it's own source.</li>
<li>Our action is to execute the desired compiler, in this case cc to generate an output executable with the name calculator .</li>
</ul>
<p>Compiled programming languages like C require us to recompile the program each time we change the source code. While the program keeps being simple there's no problem in rebuilding the whole program even if we only changed one file. But when we start to have a more complex program, compilation times increase, and recompiling everything just to update few changes is not effective.</p>
<p>The same way we create the target calculator we can make a target for each of the files that build it.</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="dv">calculator:</span><span class="dt"> main.o sum.o sub.o mult.o div.o</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a>    cc -o calculator main.o sum.o sub.o mult.o div.o</span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb4-4"><a href="#cb4-4" aria-hidden="true" tabindex="-1"></a><span class="dv">main.o:</span><span class="dt"> main.c main.h</span></span>
<span id="cb4-5"><a href="#cb4-5" aria-hidden="true" tabindex="-1"></a>    cc -c main.c</span>
<span id="cb4-6"><a href="#cb4-6" aria-hidden="true" tabindex="-1"></a><span class="dv">sum.o:</span><span class="dt"> sum.c sum.h</span></span>
<span id="cb4-7"><a href="#cb4-7" aria-hidden="true" tabindex="-1"></a>    cc -c sum.c</span>
<span id="cb4-8"><a href="#cb4-8" aria-hidden="true" tabindex="-1"></a><span class="dv">sub.o:</span><span class="dt"> sub.c sub.h</span></span>
<span id="cb4-9"><a href="#cb4-9" aria-hidden="true" tabindex="-1"></a>    cc -c sub.c</span>
<span id="cb4-10"><a href="#cb4-10" aria-hidden="true" tabindex="-1"></a><span class="dv">mult.o:</span><span class="dt"> mult.c mult.h</span></span>
<span id="cb4-11"><a href="#cb4-11" aria-hidden="true" tabindex="-1"></a>    cc -c main.c</span>
<span id="cb4-12"><a href="#cb4-12" aria-hidden="true" tabindex="-1"></a><span class="dv">div.o:</span><span class="dt"> div.c div.h</span></span>
<span id="cb4-13"><a href="#cb4-13" aria-hidden="true" tabindex="-1"></a>    cc -c div.c</span></code></pre></div>
<p>This method forces us to write function declarations in separated <code>.h</code> header files and definitions in <code>.c</code> files to avoid multiple definitions. But we take the advantage of building only the objects that have modified dependencies.</p>
<p><code>make()</code> checks the timestamp of the files to keep track of modifications. If an object dependency gets a timestamp that is newer than the object's timestamp, it'll recompile that object when executed.</p>
<p>We can also create rules for steps that don't involve compiling or building the program, such as placing the built program in the correct directory, removing it (the same as uninstalling) or cleaning the compiled objects.</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a><span class="ex">clean:</span> </span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a>    <span class="fu">rm</span> <span class="at">-f</span> <span class="pp">*</span>.o calculator</span></code></pre></div>
<p>Now instead of manually removing those files when we need a clean build, we can call <code>make clean</code> and the rule will perform the action.</p>
<p>To make an install rule we can follow the same procedure, just adding the binary as a dependency to the rule:</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a><span class="ex">install:</span> calculator</span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true" tabindex="-1"></a>    <span class="fu">mkdir</span>  <span class="at">-p</span> /opt/calc</span>
<span id="cb6-3"><a href="#cb6-3" aria-hidden="true" tabindex="-1"></a>    <span class="fu">cp</span> $<span class="op">&lt;</span> /opt/calc/calculator</span></code></pre></div>
<p>and the uninstall rule is a simple recipe that removes the copied file:</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a><span class="ex">uninstall:</span></span>
<span id="cb7-2"><a href="#cb7-2" aria-hidden="true" tabindex="-1"></a>    <span class="fu">rm</span> <span class="at">-f</span> /opt/calc/calculator</span></code></pre></div>
<p>The rules that don't involve compiling or building a program can get us in trouble if we ever meet the situation where an object is named like them (clean, install or uninstall in this case). To solve this, make has <code>PHONY</code> targets which are just a name for a recipe to be executed when you make an explicit request.</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a><span class="ex">.PHONY:</span> clean</span>
<span id="cb8-2"><a href="#cb8-2" aria-hidden="true" tabindex="-1"></a><span class="ex">clean:</span> </span>
<span id="cb8-3"><a href="#cb8-3" aria-hidden="true" tabindex="-1"></a>    <span class="fu">rm</span> <span class="at">-f</span> <span class="pp">*</span>.o calculator</span></code></pre></div>
<p>This way we avoid conflicts with other files.</p>
<h3 id="macros">Macros</h3>
<p>When programs start increasing the number of source files and library dependencies, the amount of objects and files to track increases. Luckily for us, make can handle this if we use macros (variables).</p>
<p>A macro has the following format:</p>
<div class="sourceCode" id="cb9"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a><span class="ex">name</span> = data</span></code></pre></div>
<p>where name is an identifier and data is the text that'll be substituted each time make sees ${name}.</p>
<p>Some predefined macros are:</p>
<ul>
<li><code>CC</code> is used to store the name of the compiler which we want to use (cc, gcc, clang, etc).</li>
</ul>
<div class="sourceCode" id="cb10"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true" tabindex="-1"></a><span class="ex">CC</span> = cc</span></code></pre></div>
<ul>
<li><code>CFLAGS</code> is used to list the flags we want the compiler to use.</li>
</ul>
<div class="sourceCode" id="cb11"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb11-1"><a href="#cb11-1" aria-hidden="true" tabindex="-1"></a><span class="ex">CFLAGS</span> = <span class="at">-c</span> <span class="at">-g</span> <span class="at">-Wall</span></span></code></pre></div>
<ul>
<li><code>LDFLAGS</code> is used to link libraries. Some header files like <code>&lt;math.h&gt;</code> are part of the system and aren't locally present in our code, but as any other header file, they contain just declarations and the compiler needs to check for the actual definitions somewhere.</li>
</ul>
<div class="sourceCode" id="cb12"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb12-1"><a href="#cb12-1" aria-hidden="true" tabindex="-1"></a><span class="ex">LDFLAGS</span> = <span class="at">-lm</span></span></code></pre></div>
<p>Similarly we can make a macro for all our source files, dependencies and objects.</p>
<div class="sourceCode" id="cb13"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb13-1"><a href="#cb13-1" aria-hidden="true" tabindex="-1"></a><span class="dt">SRC </span><span class="ch">=</span><span class="st"> main.c sum.c sub.c mult.c div.c</span></span>
<span id="cb13-2"><a href="#cb13-2" aria-hidden="true" tabindex="-1"></a><span class="dt">OBJ </span><span class="ch">=</span><span class="st"> </span><span class="ch">$(</span><span class="dt">SRC</span><span class="kw">:</span><span class="ss">.c</span><span class="kw">=</span><span class="ss">.o</span><span class="ch">)</span></span></code></pre></div>
<p>We are storing all our source files in the <code>SRC</code> macro, and since the object files share names with the source files, we are transforming the content inside <code>SRC</code> by changing the <code>.c</code> suffix with <code>.o</code> and storing it in the <code>OBJ</code> macro.</p>
<p>Source files can be huge in number, and manually typing each source file name can end up being tedious and make the line hard to work with.</p>
<p>BSD Make can store all the source files in a variable by executing a command-line operation, by using <code>!=</code>:</p>
<div class="sourceCode" id="cb14"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb14-1"><a href="#cb14-1" aria-hidden="true" tabindex="-1"></a>SRC <span class="dt">!</span><span class="ch">=</span><span class="st"> ls src/*.c</span></span></code></pre></div>
<p>and if you want to find files in subdirectories too, you can execute:</p>
<div class="sourceCode" id="cb15"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb15-1"><a href="#cb15-1" aria-hidden="true" tabindex="-1"></a><span class="dt">FILES!</span><span class="ch">=</span><span class="st"> find . -type f -name &#39;*.c&#39;</span></span></code></pre></div>
<p>On the GNU Make version we can take the advantage of wildcards to get all source files stored in a variable:</p>
<div class="sourceCode" id="cb16"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb16-1"><a href="#cb16-1" aria-hidden="true" tabindex="-1"></a><span class="dt">SRC </span><span class="ch">=</span><span class="st"> </span><span class="ch">$(</span><span class="kw">wildcard</span><span class="st"> \*.c</span><span class="ch">)</span></span></code></pre></div>
<p>Both of them which will take every <code>.c</code> file inside the current directory.</p>
<p>Note that the value for SRC is encapsulated between brackets and includes the explicit wildcard word. If we just associate <code>src</code> to <code>*.c</code> it will store the literal set of characters and won't behave as expected.</p>
<p>Source files may happen to be in different directories. In that case we only need to repeat the wildcard process:</p>
<div class="sourceCode" id="cb17"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb17-1"><a href="#cb17-1" aria-hidden="true" tabindex="-1"></a><span class="dt">SRC </span><span class="ch">=</span><span class="st"> </span><span class="ch">$(</span><span class="kw">wildcard</span><span class="st"> src/*.c</span><span class="ch">)</span><span class="st"> </span><span class="ch">$(</span><span class="kw">wildcard</span><span class="st"> src/modules/*.c</span><span class="ch">)</span></span></code></pre></div>
<p>Macros don't need to be upper case, and can be used arbitrarily to simplify name repetitions like our program's name:</p>
<div class="sourceCode" id="cb18"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb18-1"><a href="#cb18-1" aria-hidden="true" tabindex="-1"></a><span class="dt">prog_name </span><span class="ch">=</span><span class="st"> calculator</span></span></code></pre></div>
<p>Our <code>Makefile</code> can be transformed in something cleaner:</p>
<div class="sourceCode" id="cb19"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb19-1"><a href="#cb19-1" aria-hidden="true" tabindex="-1"></a><span class="dt">CC </span><span class="ch">=</span><span class="st"> cc</span></span>
<span id="cb19-2"><a href="#cb19-2" aria-hidden="true" tabindex="-1"></a><span class="dt">CFLAGS </span><span class="ch">=</span><span class="st"> -c -g -Wall</span></span>
<span id="cb19-3"><a href="#cb19-3" aria-hidden="true" tabindex="-1"></a><span class="dt">LDFLAGS </span><span class="ch">=</span><span class="st"> -lm</span></span>
<span id="cb19-4"><a href="#cb19-4" aria-hidden="true" tabindex="-1"></a>SRC <span class="dt">!</span><span class="ch">=</span><span class="st"> ls src/*.c </span></span>
<span id="cb19-5"><a href="#cb19-5" aria-hidden="true" tabindex="-1"></a><span class="dt">OBJ </span><span class="ch">=</span><span class="st"> </span><span class="ch">${</span><span class="dt">SRC</span><span class="kw">:</span><span class="ss">.c=.o</span><span class="ch">}</span></span>
<span id="cb19-6"><a href="#cb19-6" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb19-7"><a href="#cb19-7" aria-hidden="true" tabindex="-1"></a><span class="dt">prog_name </span><span class="ch">=</span><span class="st"> calculator</span></span>
<span id="cb19-8"><a href="#cb19-8" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb19-9"><a href="#cb19-9" aria-hidden="true" tabindex="-1"></a><span class="dv">calculator:</span><span class="dt"> </span><span class="ch">${</span><span class="dt">OBJ</span><span class="ch">}</span></span>
<span id="cb19-10"><a href="#cb19-10" aria-hidden="true" tabindex="-1"></a>    <span class="ch">${</span><span class="dt">CC</span><span class="ch">}</span> -o <span class="ch">${</span><span class="dt">prog_name</span><span class="ch">}</span> <span class="ch">${</span><span class="dt">OBJ</span><span class="ch">}</span> <span class="ch">${</span><span class="dt">LDFLAGS</span><span class="ch">}</span></span>
<span id="cb19-11"><a href="#cb19-11" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb19-12"><a href="#cb19-12" aria-hidden="true" tabindex="-1"></a><span class="dv">main.o:</span><span class="dt"> main.c main.h</span></span>
<span id="cb19-13"><a href="#cb19-13" aria-hidden="true" tabindex="-1"></a>    <span class="ch">${</span><span class="dt">CC</span><span class="ch">}</span> -c main.c</span>
<span id="cb19-14"><a href="#cb19-14" aria-hidden="true" tabindex="-1"></a><span class="dv">sum.o:</span><span class="dt"> sum.c sum.h</span></span>
<span id="cb19-15"><a href="#cb19-15" aria-hidden="true" tabindex="-1"></a>    <span class="ch">${</span><span class="dt">CC</span><span class="ch">}</span> -c sum.c</span>
<span id="cb19-16"><a href="#cb19-16" aria-hidden="true" tabindex="-1"></a><span class="dv">sub.o:</span><span class="dt"> sub.c sub.h</span></span>
<span id="cb19-17"><a href="#cb19-17" aria-hidden="true" tabindex="-1"></a>    <span class="ch">${</span><span class="dt">CC</span><span class="ch">}</span> -c sub.c</span>
<span id="cb19-18"><a href="#cb19-18" aria-hidden="true" tabindex="-1"></a><span class="dv">mult.o:</span><span class="dt"> mult.c mult.h</span></span>
<span id="cb19-19"><a href="#cb19-19" aria-hidden="true" tabindex="-1"></a>    <span class="ch">${</span><span class="dt">CC</span><span class="ch">}</span> -c mult.c</span>
<span id="cb19-20"><a href="#cb19-20" aria-hidden="true" tabindex="-1"></a><span class="dv">div.o:</span><span class="dt"> div.c div.h</span></span>
<span id="cb19-21"><a href="#cb19-21" aria-hidden="true" tabindex="-1"></a>    <span class="ch">${</span><span class="dt">CC</span><span class="ch">}</span> -c div.c</span>
<span id="cb19-22"><a href="#cb19-22" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb19-23"><a href="#cb19-23" aria-hidden="true" tabindex="-1"></a><span class="ot">.PHONY:</span><span class="dt"> clean</span></span>
<span id="cb19-24"><a href="#cb19-24" aria-hidden="true" tabindex="-1"></a><span class="dv">clean:</span><span class="dt"> </span></span>
<span id="cb19-25"><a href="#cb19-25" aria-hidden="true" tabindex="-1"></a>    rm -f *.o <span class="ch">${</span><span class="dt">prog_name</span><span class="ch">}</span></span>
<span id="cb19-26"><a href="#cb19-26" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb19-27"><a href="#cb19-27" aria-hidden="true" tabindex="-1"></a><span class="ot">.PHONY:</span><span class="dt"> install</span></span>
<span id="cb19-28"><a href="#cb19-28" aria-hidden="true" tabindex="-1"></a><span class="dv">install:</span><span class="dt"> </span><span class="ch">${</span><span class="dt">prog_name</span><span class="ch">}</span></span>
<span id="cb19-29"><a href="#cb19-29" aria-hidden="true" tabindex="-1"></a>    mkdir  -p /opt/calc</span>
<span id="cb19-30"><a href="#cb19-30" aria-hidden="true" tabindex="-1"></a>    cp <span class="ch">$&lt;</span> /opt/calc/calculator</span>
<span id="cb19-31"><a href="#cb19-31" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb19-32"><a href="#cb19-32" aria-hidden="true" tabindex="-1"></a><span class="ot">.PHONY:</span><span class="dt"> uninstall</span></span>
<span id="cb19-33"><a href="#cb19-33" aria-hidden="true" tabindex="-1"></a><span class="dv">uninstall:</span></span>
<span id="cb19-34"><a href="#cb19-34" aria-hidden="true" tabindex="-1"></a>    rm -f /opt/calc/calculator</span></code></pre></div>
<p><code>make()</code> can figure out that we want an object file from a source file as it has an implicit rule for updating an object .o file from a correspondingly named .c file using a cc -c command.</p>
<div class="sourceCode" id="cb20"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb20-1"><a href="#cb20-1" aria-hidden="true" tabindex="-1"></a><span class="fu">cc</span> <span class="at">-c</span> main.c <span class="at">-o</span> main.o</span></code></pre></div>
<p>The source <code>.c</code> file is automatically added to the dependencies, so we can reduce our rule:</p>
<div class="sourceCode" id="cb21"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb21-1"><a href="#cb21-1" aria-hidden="true" tabindex="-1"></a><span class="dv">main.o:</span><span class="dt"> main.c main.h</span></span>
<span id="cb21-2"><a href="#cb21-2" aria-hidden="true" tabindex="-1"></a>    <span class="ch">${</span><span class="dt">CC</span><span class="ch">}</span> -c main.c</span></code></pre></div>
<p>letting it appear as:</p>
<div class="sourceCode" id="cb22"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb22-1"><a href="#cb22-1" aria-hidden="true" tabindex="-1"></a><span class="dv">main.o:</span><span class="dt"> main.h </span></span></code></pre></div>
<p>Chances are that when building a program with make we get an error like this:</p>
<div class="sourceCode" id="cb23"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb23-1"><a href="#cb23-1" aria-hidden="true" tabindex="-1"></a><span class="ex">cannot</span> find file <span class="st">&quot;sum.h&quot;</span></span></code></pre></div>
<p>telling us that some required header isn't found. We can tell make where to look for prerequisites using the <code>VPATH</code> macro.</p>
<p>The value of <code>VPATH</code> specifies a list of directories that make should search expecting to find prerequisite files and rule targets that are not in the current directory.</p>
<div class="sourceCode" id="cb24"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb24-1"><a href="#cb24-1" aria-hidden="true" tabindex="-1"></a><span class="dt">VPATH </span><span class="ch">=</span><span class="st"> /inc /modules/inc</span></span></code></pre></div>
<blockquote>
<p>Note that <code>VPATH</code> will look through the directories list in the order we write them from left to right.</p>
</blockquote>
<p>Another option to look for prerequisites is telling the compile where to look for them using the <code>-I</code> flag which indicates a directory where the requested code should be:</p>
<div class="sourceCode" id="cb25"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb25-1"><a href="#cb25-1" aria-hidden="true" tabindex="-1"></a><span class="ch">-</span><span class="fu">I/src/inc</span></span></code></pre></div>
<p>and should be included in the <code>CFLAGS</code> macro.</p>
<p>We can take our example and clean it with the new shown resources:</p>
<div class="sourceCode" id="cb26"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb26-1"><a href="#cb26-1" aria-hidden="true" tabindex="-1"></a><span class="dt">CC </span><span class="ch">=</span><span class="st"> cc</span></span>
<span id="cb26-2"><a href="#cb26-2" aria-hidden="true" tabindex="-1"></a><span class="dt">INC </span><span class="ch">=</span><span class="st"> -Iinclude</span></span>
<span id="cb26-3"><a href="#cb26-3" aria-hidden="true" tabindex="-1"></a><span class="dt">CFLAGS </span><span class="ch">=</span><span class="st"> -c -g -Wall </span><span class="ch">${</span><span class="dt">INC</span><span class="ch">}</span></span>
<span id="cb26-4"><a href="#cb26-4" aria-hidden="true" tabindex="-1"></a><span class="dt">LDFLAGS </span><span class="ch">=</span><span class="st"> -lm</span></span>
<span id="cb26-5"><a href="#cb26-5" aria-hidden="true" tabindex="-1"></a>SRCS <span class="dt">!</span><span class="ch">=</span><span class="st"> ls src/*.c</span></span>
<span id="cb26-6"><a href="#cb26-6" aria-hidden="true" tabindex="-1"></a><span class="dt">OBJ </span><span class="ch">=</span><span class="st"> </span><span class="ch">${</span><span class="dt">SRC</span><span class="kw">:</span><span class="ss">.c=.o</span><span class="ch">}</span></span>
<span id="cb26-7"><a href="#cb26-7" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb26-8"><a href="#cb26-8" aria-hidden="true" tabindex="-1"></a><span class="dt">prog_name </span><span class="ch">=</span><span class="st"> calculator</span></span>
<span id="cb26-9"><a href="#cb26-9" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb26-10"><a href="#cb26-10" aria-hidden="true" tabindex="-1"></a><span class="dv">calculator:</span><span class="dt"> </span><span class="ch">${</span><span class="dt">OBJ</span><span class="ch">}</span></span>
<span id="cb26-11"><a href="#cb26-11" aria-hidden="true" tabindex="-1"></a>    <span class="ch">${</span><span class="dt">CC</span><span class="ch">}</span> -o <span class="ch">${</span><span class="dt">prog_name</span><span class="ch">}</span> <span class="ch">${</span><span class="dt">OBJ</span><span class="ch">}</span> <span class="ch">${</span><span class="dt">LDFLAGS</span><span class="ch">}</span></span>
<span id="cb26-12"><a href="#cb26-12" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb26-13"><a href="#cb26-13" aria-hidden="true" tabindex="-1"></a><span class="dv">${OBJS}:</span><span class="dt"> </span><span class="ch">${</span><span class="dt">SRCS</span><span class="ch">}</span></span>
<span id="cb26-14"><a href="#cb26-14" aria-hidden="true" tabindex="-1"></a>    <span class="ch">${</span><span class="dt">CC</span><span class="ch">}</span> <span class="ch">${</span><span class="dt">CFLAGS</span><span class="ch">}</span> -c <span class="ch">$&lt;</span> -o <span class="ch">$@</span></span>
<span id="cb26-15"><a href="#cb26-15" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb26-16"><a href="#cb26-16" aria-hidden="true" tabindex="-1"></a><span class="ot">.PHONY:</span><span class="dt"> clean</span></span>
<span id="cb26-17"><a href="#cb26-17" aria-hidden="true" tabindex="-1"></a><span class="dv">clean:</span><span class="dt"> </span></span>
<span id="cb26-18"><a href="#cb26-18" aria-hidden="true" tabindex="-1"></a>    rm -f *.o <span class="ch">${</span><span class="dt">prog_name</span><span class="ch">}</span></span>
<span id="cb26-19"><a href="#cb26-19" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb26-20"><a href="#cb26-20" aria-hidden="true" tabindex="-1"></a><span class="ot">.PHONY:</span><span class="dt"> install</span></span>
<span id="cb26-21"><a href="#cb26-21" aria-hidden="true" tabindex="-1"></a><span class="dv">install:</span><span class="dt"> </span><span class="ch">${</span><span class="dt">prog_name</span><span class="ch">}</span></span>
<span id="cb26-22"><a href="#cb26-22" aria-hidden="true" tabindex="-1"></a>    mkdir  -p /opt/calc</span>
<span id="cb26-23"><a href="#cb26-23" aria-hidden="true" tabindex="-1"></a>    cp <span class="ch">$&lt;</span> /opt/calc/calculator</span>
<span id="cb26-24"><a href="#cb26-24" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb26-25"><a href="#cb26-25" aria-hidden="true" tabindex="-1"></a><span class="ot">.PHONY:</span><span class="dt"> uninstall</span></span>
<span id="cb26-26"><a href="#cb26-26" aria-hidden="true" tabindex="-1"></a><span class="dv">uninstall:</span></span>
<span id="cb26-27"><a href="#cb26-27" aria-hidden="true" tabindex="-1"></a>    rm -f /opt/calc/calculator</span></code></pre></div>
<p>Now we only need to save the file and execute make calling the desired command. To build the calculator program it'd be:</p>
<div class="sourceCode" id="cb27"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb27-1"><a href="#cb27-1" aria-hidden="true" tabindex="-1"></a><span class="ex">$</span> make calculator <span class="kw">&amp;&amp;</span> <span class="fu">install</span></span></code></pre></div>
<h3 id="comments">Comments</h3>
<p>Comments are pretty much self explanatory. They are lines of text that as in programming languages, do nothing but provide useful information or reminders.</p>
<p>We can place comments around our <code>Makefile</code> by using the hashtag <code>#</code> symbol. Anything after a <code>#</code> will be ignored.</p>
<div class="sourceCode" id="cb28"><pre class="sourceCode makefile"><code class="sourceCode makefile"><span id="cb28-1"><a href="#cb28-1" aria-hidden="true" tabindex="-1"></a><span class="co"># An example comment</span></span></code></pre></div>
<h2 id="summing-up">Summing up</h2>
<p>In addition to compiling and building our own C/C++ code, working inside a BSD system involves being working close with its source code, and most of the times we have to compile and build packages from ports. That process works the same way so you can now start tweaking and inspecting source Makefiles each time you need to change or install a program. It'll grant you access to custom install instructions specific for your machine.</p>
<p>We can do more things with make like building install menus, compiling libraries and including Makefiles inside other Makefiles. All those topics need a dedicated article for each of them.</p>
<p>There's an official manual for GNU Make that you can read for advanced knowledge in the tool, although staying POSIX is always a recommended thing.</p>
	</div>
	<footer>
		<div class="footer-grid">
			<div class="footer-info">
			<p>2025 - n0madcoder<br>
			<a href="mailto:nomadcoder@vertexfarm.xyz">nomadcoder@vertexfarm.xyz</a></p>
			</div>
			<div class="footer-social">
				<a href="https://t.me/s/vertexfarm">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm-3.11-8.83l.013-.007.87 2.87c.112.311.266.367.453.341.188-.025.287-.126.41-.244l1.188-1.148 2.55 1.888c.466.257.801.124.917-.432l1.657-7.822c.183-.728-.137-1.02-.702-.788l-9.733 3.76c-.664.266-.66.638-.12.803l2.497.78z"/></svg>
				</a>
				<a href="https://buymeacoffee.com/n0madcoder">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M5 3h15a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-2v3a4 4 0 0 1-4 4H8a4 4 0 0 1-4-4V4a1 1 0 0 1 1-1zm13 2v3h2V5h-2zM2 19h18v2H2v-2z"/></svg>
				</a>
			</div>
		</div>
	</footer>
</body>

</html>
