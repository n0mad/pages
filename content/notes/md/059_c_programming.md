---
title: C Programming | Event notifications
date: November 19, 2023
tgdate: 2023-11-19
shortdesc: Fundamentals of event oriented programming with kqueue in c
tgdesc: 
tgimg: 000_freebsd_gearingup_art.png
---

# C Programming | Event notifications

some intro here

something that tells you when stuff happens.

## About kqueue and its counterparts

Kernel queue(s) are a solution for notifying that something happened or a condition is met in a generic way.

On the philosophy of using kqueue for FS monitoring: I see how kqueue has great purposes, but it strikes me as its intent was to monitor sockets more so that files. Extending this use to directories/files seems to me like a bit of a hack, but I suppose it works well enough for a small set. When it is scaled to such a large number as would be used in several users' Plex libraries, it looks far more like it is being used for a task it was never designed to handle. Even more so when these libraries are mostly quiescent with occasional additions/subtractions/modifications of files/directories. Holding hundreds of thousands of fds open for what is likely a 10s of changes a day seems excessive. It strikes me that FreeBSD really needs some kernel API that's truly designed for file system monitoring over using an existing API that seems ill-suited for this scale.

On inotify: Personally, I'd prefer FSEvents over inotify as inotify still requires opening an fd-like object for every directory. It does give information about changes to files contained within the directory though and provides path information on the changed item. This does reduce the amount of accounting the application must perform but there's still some for every directory. The FSEvents monitors an entire directory tree at once and provides rich information in its callback. This is much easier on the developer.

To clarify my comparisons of kqueue to inotify/fsevents I'll give a quick and dirty overview of the process for monitoring a file-system tree:
Setup:
With kqueue, the user-space developer must:

    Enumerate the entire directory structure recursively
    Open an FD for every directory and file
    Add accounting to lookup by FD as well as by path (needed later)

With inotify:

    Create inotify context
    Enumerate the entire directory structure recursively
    Register a watch in the context for every directory (not files)
    Add accounting to lookup path by watch id (needed later)

With fsevents:

    Register a single watch for the directory tree (no recursion)

Watching for changes:
With kqueue:

    Loop over kevent giving it a giant array of fds from the accounting to get list of FDs changed
    For each FD indicating change:
        Lookup in accounting by FD
        If a directory changed, scan dir to figure out what the change was
        Use the by path accounting to determine file/directory additions and removals
        If a file/directory was added, recursively add them (as in setup above)
        If a file/directory was removed, recursively remove them from accounting and close the fds

With inotify:

    Loop over read of inotify context
    For each object read:
        Lookup the path by watch id (provided in object) and add file/dir name (also provided) to get full path of change
        Take action on the path in the object
        If a directory was added, recursively add directories (as in setup above)
        If a directory was removed, recursively remove directories from accounting and unregister with the context

With fsevents:

    Read the path of the updated file/directory given in the callback
    Take action on this path

## Event oriented programming with kqueue



## Disclaimer

I often skip this part when making notes since most of the resources I find to work with are generic, mostly man pages and standard C books that get self-referenced through the text. But this note is a bit different, I'm trying to consolidate the usage of `kqueue(2)`` in a *hacky* way to make it work similar as `inotify(1)` in Linux systems, and there's little knowledge out there about it.

The resources and references used here (listed below) have been carefuly curated (some of them ressurected thanks to the WayBack machine) so you can have everything at hand.

### References

