---
title: Command line Git | Quick guide
date: March 10, 2020
shortdesc: Feel comfortable using Git from the command line.
tgdesc: In this article guide the goal is to learn how to use Git from the command line. We will go through the different parts of a git based workflow, covering the most used ones.
---

# Command line Git | Quick guide

There are a lot of graphical interfaces to interact with Git with cozy buttons and windows. There's also a fast and powerful alternative: using Git directly from the command line.

> There are different version control systems like Mercurial, Subversion or Bazaar. We are using Git in this guide.

## What is Git?

Git is a powerful tool to maintain our code projects up to date and keep track of the changes we've made. Using it from the command-line is not that complicated as it could be seen.

To verify that we have `git(1)` installed in our system, ask for it's version in the command line:

```sh
$ git --version
```

If the result is similar to `git version 0.00.0` we are good to go. If not, just grab the package into your system:

```sh
$ doas pkg install git
```

— The first thing to perform after installing git is to set a username and an email address since git commit uses that information every single time.

```sh
$ git config --global user.name "Your Name"
$ git config --global user.email yourname@example.com
```

The `--global` flag is useful if we don't want to write the credentials each time we want to perform an action inside Git, since Git will always use that information for anything you do on that system.

> You can change the default git editor with the `git config --global core.editor` command.

In order to override global settings a different name or email address for specific projects, we can run the command without the `--global` option when working in that project.

— To check our actual settings we only need to ask git for them:

```sh
$ git config --list
```

> If you want to check where are these variables stored, usually at your `$HOME` directory you can find a file named `.gitconfig`.

## Create a repository from a local directory

If we have a recently started project that starts growing up and we decide to upload it into a git hosting service we have two options.

— The first one is to create a repository in the hosting service, initialize it with a `README.md`, clone it in our local drive, and then move all our project inside the cloned repository. This is pretty much self explanatory.

— The second one is to tell git to get our actual project file and upload it into an empty repository hosted in our git service.

The second option is pretty easy to achieve:

* We need to create an empty repository in our git hosting service (github, gitlab, codeberg...) without initializing it.

* Both names (the project directory name and the git repository name) have to match.

* In our local repository we have to initialize git.

```sh
$ git init
```

* After initializing git we have to add the content and commit our action

```sh
$ git add -A
$ git commit -m "commit message"
```

* Now we have to remotely add our git origin, which is our newly cloud created repository.

```sh
$ git remote add origin https://githost.com/username/repository.git
```

* The final step is pushing the content to the origin master branch.

```sh
$ git push -u origin master
```

We'll be asked for our git hosting credentials when pushing content.

## Select what to upload

Chances are that we have come files inside our local project that we don't want to upload, like temporary files that the system creates or test builds that serve for debugging purposes.

— We can create a special file for git that allow us to specify which content to omit when pushing the project to the git service.

This file needs to be named `.gitignore` and it's a good idea to create it in the top level directory of our repository.

Git uses *globbing* patterns to match against file names. We can construct our patterns using a set of symbols:

* `**` A pattern with a double asterisk will match directories anywhere in the repository.

```sh
**/debug
```
* `*.` A patter with an asterisk will match zero or more characters anywhere in the repository.

```sh
*.o *.log
```

* `!` Prepending an exclamation mark to a pattern negates it. A file will not be ignored if it matches a pattern, but also matches a negating pattern defined later in the file.

```sh
!important.log
```

## Commit and Push our content

Once we've made changes locally to our code or project, we need to merge them into the hosted git repository.

First we need to tell git to add our changes. We can add all the files with the flag `-A` or all the modified files with the flag `.`

```sh
# add all files in the repository
$ git add -A

# add all modified files in the repository
$ git add .
```

The following step is to commit the changes, usually with a comment.

```sh
$ git commit
```

If we want to make a one line comment we can add the flag -m after commit and write inside double quotes our message:

```sh
$ git commit -m "Updated foo.c -Changed boo function -Removed trash"
```

In the short run, we are most likely going to remember what we did in that commit. A lot of commit messages are similar to "update code" or "wrote function boo".

In the long run, you're going to love the time spent writing the commit messages with detail and common sense. Here's a short template of how can we structure a commit message:

```sh
Summarize the change in a few but meaningful words.

Additions:
- what you added

Fixes:
- what it fixes

Changes:
- what it changes
```

Longer explanation if needed goes here, along with additional notes, or relevant info.

When typing git commit without the -m flag, the shell will open the default text editor and will ask us to write the commit message.

## Pull changes to our local folder

Every time we access the repository locally, we need to keep track of the cloud updates, so the work can flow seamlessly. 

The first thing we have to do before start working inside the local repository, is to check for changes:

```sh
$ git status
```

if we have changes we can add them to our local repository via git pull:

```sh
$ git pull
```

Then we can start messing around.

— Things went nuts, the content inside the cloud repository had new changes but we were working locally without pulling them first!

Don't worry, there's a solution for that. You can stash (hide) your changes, pull and then apply your changes again:

```sh
$ git stash -u
$ git pull
$ git stash pop 
```

## Check progress via commits and branches from cli

GUI tools make pretty graphics to showcase a git project. Guess what, you can do the same with the command-line using `git log`:

```sh
git log --graph --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%an%C(reset)%C(bold yellow)%d%C(reset) %C(dim white)- %s%C(reset)' --all
```

- The `--all` flag is used to show all commits.

- The `--graph` flag is used to show the commit graph, that is, the cozy lines that represent the commit paths and progress.

- The `--format` flag is used to customize the output. In this case we show all commits in a graph with a colored commit message, author, date and the commit hash in the output.


## Copy a repository from the web

This is probably something you already know, but just for refreshing the memory, let's take a brief look at it.

When we want to get a repository from the web, we have an option to zip the entire repository and download it with just one click in the specified icon. This requires to manually unzip it later. But we are trying to use git from the command-line, and extra steps like unzip projects aren't part of the goal.

Every code repository has an `https` direction we can use to clone using git in the command-line in a very easy and quick way:

* First we need to create (or navigate to) a directory where we would like to store the git project.

* The second step is to copy that url and clone it via git clone:

```sh
$ git clone https://hosting.site/user/repo-name.git
```

## Git hosting

Project tracking in `git` is great, but we need to keep our repository somewhere. One of the solutions is to create our own server and make our own repository with or without tools like Gogs.

To create a local git repository we need to have a `git` user account with some permissions in the machine where we would like to store our project.

> From [the official Git documentation](https://git-scm.com/book/en/v2/Git-on-the-Server-Setting-Up-the-Server):

```sh
$ doas adduser git
$ su git
$ cd
$ mkdir .ssh && chmod 700 .ssh
$ touch .ssh/authorized_keys && chmod 600 .ssh/authorized_keys
```

> Remember to add your public key to the authorized_keys file. `$ cat /tmp/id_rsa.yourusername.pub >> ~/.ssh/authorized_keys`

Once done, under the `git` user `home` directory we can create a subdirectory where we would like to store our git projects, and initialize new ones with the following command:

```sh
$ mkdir /home/git/repositories
$ git init --bare --shared=group /home/git/repositories/project-name.git
```

Now from your machine you can do as follows in order to initialize the repo and start working (assuming that we are in an existing project with files in it):

```sh
# your machine
$ cd project-name
$ git init
$ git add .
$ git commit -m 'Initial commit'
$ git remote add origin git@gitserverip:/srv/git/project-name.git
$ git push origin master
```

At this point you can let the rest of the team use the git repository as usual.

```sh
$ git clone git@gitserver:/srv/git/project.git
$ cd project
$ vim README
$ git commit -am 'Fix for README file'
$ git push origin master
```

> The workflow examples have been extracted from the official Git documentation. Refer to that for further and deeper explanations on how to use Git.

— Another way is to go online and register in a git hosting service. The most popular out there is GitHub. Since Microsoft acquired it, is becoming a social hub where developers share code, follow each others, post updates and sponsor projects they like. There's nothing wrong going GitHub. Just be sure to read the terms & conditions carefully if you're concern about privacy, and don't forget to license your code.

Luckily there are alternatives to GitHub. All the following services provide options to store your code freely and the ability to decide whether your code is public or private.

* Gitea is based on Gogs. It's offered as a self-hosted service but you can use an already free hosted service at gitea.com
* Codeberg is great to store open source projects. It's based on Gitea.
* NotABug is based on Gogs and offers free code hosting for any project that is distributed under any free license.
* Gitlab is a commercial git service that offers enterprise ready tools and also a free plan where you can store your code and take advantage of a limited set of their tools.
* Bitbucket is another commercial git service aimed for teams and big projects. If you work solo or your project is less than five persons, you can opt for a free plan.

## Summing up

Git is way more complex and powerful than just the content we read here. There's no point in copy-pasting complex workflows and custom needs in a guide that pretends to explain core common things and be useful in a hurry .

If you want to deep dive in Git, there is an official book available to read for free online in the official Git site.
