---
title: FreeBSD gearing-up
date: January 6, 2020
tgdate: 2020-01-06
shortdesc: Configure a fresh FreeBSD installation.
tgdesc: In this quick guide article we will configure a fresh installation of FreeBSD, covering from system updates and software installations to driver configurations for graphics cards and laptop touchpads.
tgimg: 000_freebsd_gearingup_art.png
---

# FreeBSD gearing-up

<p id="sub-desc">In this quick guide article we will configure a fresh installation of FreeBSD, covering from system updates and software installations to driver configurations for graphics cards and laptop touchpads.</p>

> This article is tailored and wrapped based on personal experience. It's not supposed to replace official documentation. If in doubt, go check [the Handbook](https://docs.freebsd.org/en/books/handbook/).

By following the steps presented here I hope you can start feeling home or at least comfortable in your new work environment, and also you start getting confident with the system.

## Know about your system

<p id="sub-desc">After a fresh install of the FreeBSD os, even tho you may now your hardware pretty well it's a good idea to check it out how the OS describes it and which parts it detects.</p>

There are several commands that can help us out with this task. Here are some of them:

- `sysctl hw.model` - should get the CPU model.

- `sysctl hw.ncpu` - should get the number of CPUs.

- `sysctl hw.physmem` - should get the amount of RAM in bytes.

- `pciconf -lvbce` - should list all the PCI devices.

The command `dmesg` will list some logs from the system boot, but there are also files that contain useful information about the system. Here are some of them:

- `/var/log/messages` - contains all the system logs from the boot to the shutdown.

- `/var/log/Xorg.0.log` - contains the X server logs.


## Add your user to groups

<p id="sub-desc">Groups are lists of users. They have the ability to encapsulate permissions among other stuff. In FreeBSD we can use the `id(1)` and `pw(8)` commands to view, edit, create, and remove and users and groups.</p>

Some useful groups are `wheel`, `operator` and `video`.

- `wheel` is a special user group used to control access to the `su` command.

- `operator` gives permissions to interact with real devices. It has read permissions on `/dev/geom.ctl`.

- `video` grants access of the members of that group to the `/dev/dri` (direct rendering infrastructure) which is used for GPU hardware acceleration.

Before making any change check your user's current membership by typing:

```sh
$ id your_user_name
```

That will list you which groups you're part of. Then to maintain your groups (if any) and add new ones, use the following command:

```sh
$ doas pw group mod GROUP -m your_user_name
```

> Change `GROUP` with the desired group name.

To remove a user from a group, the command would be the same as before, but with a `-d` flag instead of the `-m` one:

```sh
$ doas pw group mod GROUP -d your_user_name
```

## Turn off "beep" sounds

<p id="sub-desc">By default FreeBSD has console beeps enabled. This shouldn't be such a deal, but it becomes a bit annoying when working in the `tty`.</p>

In order to turn off your computer's bell on the command line type:

```sh
$ doas sysctl kern.vt.enable_bell=0
```

To make it permanent on every system boot, save it to your `/etc/sysctl.conf` file.

```sh
$ doas echo "kern.vt.enable_bell=0" >> /etc/sysctl.conf
```

## Update the system

<p id="sub-desc">FreeBSD way to update RELEASE and STABLE versions is pretty simple and only
takes two commands: `fetch` and `install`.</p>

You can do this two steps combined, but just to keep things in order first fetch the updates, and then install them.

```sh
$ doas freebsd-update fetch
$ doas freebsd-update install
```

### Updating CURRENT

Chances are you're running a `CURRENT` version of FreeBSD where updating needs to be made by compiling the source. The Handbook explains it really in depth, but for quick purposes, here are the basic steps required to update from source:

> Since FreeBSD 13.0 the version control has been moved from svn to git.

`CURRENT` source is located at `/base/head` in FreeBSD's repositories so we need to look for it and locate into our `/usr/src` directory:

```sh
$ cd /usr/src
$ doas git remote --v
$ doas git pull /usr/src
```

Once the source code is fetched, the following steps provide a complete system upgrade:

> Note the `-jN` flag sets the number of jobs. This can be useful to speed up builds in multi-core processors. You can determine the number of cores present in your machine by reading the value of `sysctl hw.ncpu`.


```sh
$ doas git clone --branch main https://git.FreeBSD.org/src.git /usr/src
$ cd /usr/src
$ doas make -j4 buildworld
$ doas make -j4 buildkernel
$ doas make installkernel
$ shutdown -r now
$ cd /usr/src
$ doasmake installworld
$ shutdown -r now
$ doas mergemaster -Ui
```

At this point you might want to check for obsolete files or directories after the update:

```sh
$ doas make check-old
$ doas make delete-old
$ doas make check-old-libs
$ doas make delete-old-libs
$ shutdown -r now
```

### Update packages

After any update installation you can reboot the system and proceed to update the pkg collection:

```sh
$ doas pkg update
```

If this is the first time we type `pkg` in the command line, FreeBSD will ask us
to install the base. Just hit `Yes` and continue.

Packages are offered in two branches: Quarterly and Latest.

If you want to use the latest pkg branch instead of the quarterly one, follow this three steps:

1. Copy `/etc/pkg/FreeBSD.conf` to `/usr/local/etc/pkg/repos/FreeBSD.conf`
2. Change the string `quarterly` to `latest` in the `url` line
3. Run `pkg update -f` to update from the new repository metadata.

### Update ports

If you want to use the ports way to install software, you may also update the ports collection following these steps:

- Get the latest snapshot:

```sh
$ doas git clone --depth 1 https://git.FreeBSD.org/ports.git /usr/ports
```

- Update the repo after the initial checkout and as needed:

```sh
$ doas git -C /usr/ports pull
```

## Configure doas

<p id="sub-desc">`doas(1)` is the utility of choice in BSD environments to execute a given command as another user. If you come from the Linux world, this is similar to `sudo`.</p>

After installing the `doas(1)` package you have to create a `doas` configuration file in `usr/local/etc/`.

```sh
$ doas touch usr/local/etc/doas.conf
```

Using a text editor like `ee` add the following line in order to get permissions for your user (make sure your user is in the WHEEL group):

```sh
permit persist keepenv :wheel
```

## Configure firewall

<p id="sub-desc">Having a firewall in any OS is kinda mandatory. For FreeBSD we can use `pf(4)`. It's a packet filtering engine borrowed from OpenBSD that comes with the base installation.</p>

> keep in mind that FreeBSD's version of PF has diverged substantially from the upstream OpenBSD version over the years. Not all features work the same way on FreeBSD as they do in OpenBSD and vice versa.

In order to configure it, we have to create a rule configuration first:

```sh
$ doas vim /etc/pf.conf
```

Then we can populate the file with some basic and quick rules:

```sh
block in all
pass out all keep state
pass quick on lo0 all
```

Once we are done, we have to tell the system where to look for it. Open the file `/etc/rc.conf` with vim and add the following:

```sh
pf_enable="YES"
pf_rules="/etc/pf.conf"
pf_flags=""
pflog_enable="YES"
pflog_logfile="/var/log/pflog"
pflog_flags=""
```

> You can add your own flags to it, be sure to check `pf(4)` man pages to get
> more info about the tool.

## Managing packages

<p id="sub-desc">Package management inside FreeBSD is done via `pkg(8)`. You probably used it before to install `doas(1)` in the system.</p>

> After a fresh install of FreeBSD, we are dropped into a tty, with no GUI desktop. There is a guide on how to rice a FreeBSD desktop in this site that you may follow to set up a graphical environment.

### Install packages

Packages are the already compiled ports in binary form that you can easily add to your system.

- The command to install packages is really simple. We have been using it through this guide. Just type `pkg install` followed by the package you want to install.

> In order to install packages we need elevated permissions. Use `doas(1)` to perform the action.

```sh
$ doas pkg install package_name
```

- In order to know if we already have a package installed we can perform a command line pipe, using the `pkg(8)` tools:

```sh
$ pkg info | grep package_name
```

- If we don't know the exact name of a package, or we don't know if the package is available at all in FreeBSD, we can search for it from the command line using the `pkg(8)` tools too:

```sh
$ pkg search keyword
```

- We can also know about the dependencies of a package. To do so we can ask for them in the search instruction including the `-d` flag:

```sh
$ pkg search -d package_name
```

### Update packages

You can check packages that need to be updated running `pkg upgrade -n`.

### Remove packages

Deleting packages can be done using `pkg delete package_name`.

In order to remove orphan dependencies after uninstalling a package can be done with `pkg autoremove`.

## Set-up graphic cards

<p id="sub-desc">You need to install `drm-kmod` in order to get graphic drivers working, and enable GPU acceleration.</p>

For video cards driver packages, check which brand is mounted in your system. It can be intel, AMD or nvidia, and then install the according ones:

* intel / amd: `$ doas pkg install libdrm drm-kmod`
* nvidia: `$ doas pkg install nvidia-driver`

Once you have installed the required pkg, write the following in your `/etc/rc.conf` file:

* For amdgpu graphics: `kld_list="amdgpu"`
* For Intel graphics: `kld_list="i915kms"`
* For radeonkms graphics (until 2013 Sea Islands included): `kld_list="radeonkms"`

> We have to ensure that all users requiring graphics are members of the `video` group.

If you want to use OpenCL with AMD graphics or make AMD GPUs to work, some workarounds are available:

1. Be sure you install the correct `drm-kmod` for your system. In order to get newer AMD cards to work properly you may need to install a CURRENT version of FreeBSD (not so stable as a RELEASE).
2. In laptops check if your hardware mounts an hybrid intel processor + amd gpu card and determine to select the second one.
3. In the `/boot/loader.conf` file add the following line:

```sh
hw.syscons.disable=1
```

Some graphic cards need a custom `X11` config file in order to tailor specific settings like screen tearing or desired DRI. You can place it at `10-xxgpu.conf`:

> Remember to change the values with your actual gpu card identifier and driver.

``` sh
Section "Device"
	Identifier "CardIdentifier"
	Driver "CardDriver"
	Option "DRI" "3"
	Option "TearFree" "true"
EndSection
```

In order to get effect on the `X11` config, you need to install the `X.org` display driver for your gpu card.

* For amdgpu cards: `xf86-video-amdgpu`
* For intel graphics: `xf86-video-intel`

Otherwise `X11` may not start at all.

## Enable microprocessor updates

<p id="sub-desc">Microcode updates intro</p>

Add the following line to `/etc/rc.conf`:
```sh
microcode_update_enable="YES"
```
Then for the first time execute:

```sh
doas service microcode_update start
```

You can also enable thermal sensors for your chip adding the following line to the `/boot/loader.conf` file:

* AMD K8, K10, K11 thermal sensors: `amdtemp_load="YES"`
* intel core thermal sensors: `coretemp_load="YES"`

## Working with disks

<p id="sub-desc">Formatting a disk, making partitions to it, checking its format and available space, all from the command-line is possible, fast, and really awesome.</p>

First we need to find what devices we have connected to the system. There are several methods that provide different information about.

* `$ egrep 'ada[0-9]' /var/run/dmesg.boot` Returns a simple list.

```sh
$ egrep 'ada[0-9]' /var/run/dmesg.boot
ada0 at ahcich1 bus 0 scbus0 target 0 lun 0
ada0: <KINGSTON SHFS37A120G 608ABBF0> ATA8-ACS SATA 3.x device
ada0: Serial Number 50026B77630DE4E5
ada0: 600.000MB/s transfers (SATA 3.x, UDMA6, PIO 512bytes)
ada0: Command Queueing enabled
ada0: 114473MB (234441648 512 byte sectors)
```

* `$ geom disk list` Returns more detailed list.

```sh
$ geom disk list
Geom name: ada0
Providers:
1. Name: ada0
   Mediasize: 120034123776 (112G)
   Sectorsize: 512
   Mode: r3w3e5
   descr: KINGSTON SHFS37A120G
   lunid: 50026b77630de4e5
   ident: 50026B77630DE4E5
   rotationrate: 0
   fwsectors: 63
   fwheads: 16
```

* `$ gpart show` Returns info about partitions too.

```sh
$ gpart show
=>       40  234441568  ada0  GPT  (112G)
         40     532480     1  efi  (260M)
     532520       1024     2  freebsd-boot  (512K)
     533544        984        - free -  (492K)
     534528    2097152     3  freebsd-swap  (1.0G)
    2631680  231809024     4  freebsd-zfs  (111G)
  234440704        904        - free -  (452K)
```

### Formatting a disk

Once we have detected what disk we want to work on, we can proceed to format it.

— A quick way to wipe a disk is using gpart's destroy command:

```sh
$ doas gpart destroy -F daX
```

— A brute force way to clean the disk once you have checked which devices are available and checked <u>what name they have</u>, a "brute force" way to clean the disk is to fill it with zeroes.

```sh
doas dd if=/dev/zero of=/dev/adaX bs=10M
```

Where `adaX` or `daX` is your drive name. Be careful as it is a very destructive way to work, and typing the wrong drive target can break the system.

> The label `bs=10M` is important since if you don't alter the speed, the process can take up to days depending on the disk size.

Now that we have our drive cleaned, we can proceed to create a new partition scheme:

```sh
$ doas gpart create -s GPT adaX
adaX created
```

You can check that the step worked typing `gpart show adaX`:

```sh
$ doas gpart show adaX
=>      40  10485680  adaX  GPT  (5.0G)
        40  10485680        - free -  (5.0G)
```

Next, let's add a new partition. A list with the partition types can be found at `gpart(8)` man page.

If you're planning to use the disk only in FreeBSD environments, then a partition type like `freebsd-ufs` or `freebsd-zfs` is going to perform better.

If in the other hand, we are planning to use the disk between different operating systems, a partition type like `ms-basic-data` (for later `exFAT` or `NTFS` format) is recommended.

```sh
$ doas gpart add -t PARTITION_TYPE -a 1M adaX
adaXp1 added
```

You can check that the step worked with `$ doas gpart show da0`. This time you should see a new partition added.

```sh
=>      40  10485680  adaX  GPT  (5.0G)
        40      2008        - free -  (1.0M)
      2048  10481664     1  freebsd-ufs  (5.0G)
  10483712      2008        - free -  (1.0M)
```

The final step is to give the `/dev/adaXp1` partition a format. If we chose a native partition type then our command should be like:

```sh
$ doas newfs -U /dev/adaXp1
```

To perform formatting to a partition type like `ms-basic-data` we will need some extra tools. To format an `exFAT` partition in this case we are using `exfat-utils` package:

```sh
$ doas mkfs.exfat -n volume_name /dev/adaXp1
```

Once formatting is done, let's create a directory to mount the partition:

```sh
$ doas mkdir /media/data
$ doas mount /dev/adaXp1 /media/data/
```

List the new disks with `# df` and update the `/etc/fstab` file.

```sh
$ doas vim /etc/fstab
```

Add the line that matches your new partition to the `/etc/fstab` file.

```sh
/dev/adaXp1	/media/data/		ufs	rw	2	2
```

### Mounting an external disk

If you have some external HDD drive already formatted you may want just to mount it instead of erase all the data.

> This is an example for an external NTFS drive (probably the most common formatted drive you can have if coming from Windows / MacOS or Linux). I personally prefer exFAT (the workaround is the same but with fusefs-exfat).

— First we need to install the `fusefs-ntfs` package.

```sh
$ doas pkg install fusefs-ntfs
```

After doing so, load it:

```sh
$ doas kldload fusefs
```

You can make it permanent by loading it at boot, adding the following line to `/boot/loader.conf`

```sh
fusefs_load="YES"
```

— Create a directory for your external drive inside the `/media` directory as before. Now plug your external device and get the desired partition to mount.

```sh
$ doas mkdir /media/external_drive
$ doas gpart show
```

For the example let's say we have an external drive named `da0` and a partition named `da0s1`.

To mount it, type the following:

```sh
$ doas ntfs-3g /dev/da0s1 /media/external_drive
```

— You're ready to use your external drive. Once you finish using it, in order to unmount it you just need to `umount` the directory where the disk is mounted in.

```sh
$ doas umount /media/external_drive
```

## Power Management

<p id="sub-desc">Components from your machine consume resources. One of them is current power. We can control how components manage their resource consumption manually rather than letting them use stock instructions.</p>

> On laptops it may vary from being attached to AC or to the battery.

From inside freeBSD, you can use `powerd(8)` to alter what happens when connected to AC and/or when connected to battery.

In your `/etc/rc.conf` add the following lines:

```sh
powerd_enable="YES"
powerd_flags="-a adaptive -b adaptive"
```

There are more options to set the flags. Check the `powerd(8)` man pages for more info.

An alternative to modern multi-core processors is `powerd++` (installed as `powerdxx(8)`). It's an enhanced version of `powerd(8)`. You have to choose which one to use as they cannot be running in pair.

In order to use the `powerdxx(8)` option, we also need to add some flags inside
`/etc/rc.conf`:

```sh
powerdxx_enable="YES"
performance_cx_lowest="Cmax"
economy_cx_lowest="Cmax"
```

## Async I/O

<p id="sub-desc">The `aio(4)` facility provides system calls for asynchronous I/O.</p>

In order to use it in your system, simply add the following line to the `/boot/loader.conf` file:

```sh
aio_load="YES"
```

## Setting up audio

<p id="sub-desc">As with the graphics card, to get audio working you need to load its proper driver. That can be done both in the command-line or at boot time. This is a quick recap that should go straight forward to a working result.</p>

> A deeper explanation can be found in the FreeBSD [handbook](https://www.freebsd.org/doc/handbook/sound-setup.html).

* From the command line type:

```sh
$ doas kldload snd_hda
```

* In order to load it at boot time, edit your `/boot/loader.conf` file to add the rule:

``` sh
snd_hda_load="YES"
```

The second step we need to perform is to determine which output we want to enable. Type:

``` sh
$ dmesg | grep pcm
```

in order to list which detected cards you have.

Lastly, printing `cat /dev/sndstat` displays which card is in use:

``` sh
$ cat /dev/sndstat
```

You can change it by calling `sysctl(8)`, changing *n* with the number of the desired pcm card:

``` sh
$ doas sysctl hw.snd.default_unit=n
```

To make the changes permanent, add the line into your `/etc/sysctl.conf` file:

``` sh
hw.snd.default_unit=n
```

If you're not sure which driver to load, or you have trouble getting a pcm list, change the `snd_hda` driver with `snd_driver`:

``` sh
$ doas kldload snd_driver
```

### Automatic audio I/O switch

A cool workaround that doesn't come enabled by default in some machines (since card detects hardware separately) is to have the ability to automatically switch from speakers to headphones.

* First, let's enable polling. Inside `/etc/sysctl.conf` add:

```sh
#__this make automatic switch from headphones/speaker
dev.hdac.0.polling=1
```

* Second step is to get our hardware. Based on the info from `dmesg (nid)`:

> Note that in this example speakers are `nid20` and headphones `nid21`, but your machine may have different associations.

```sh
$ doas sysctl -a | egrep "nid20|nid21" | grep original
```

* Third step is to indicate the headphone/speaker to switch and mute on connection detected. Inside the `/boot/loader.conf` type:

```sh
hint.hdaa.0.nid21.config="as=1 seq=15 device=Headphones conn=Jack"
```

### Firefox audio

You might experience some issues with Firefox and sound playback. The fastest solution to date is to ensure having installed the `sndio` package and its daemon enabled:

* Verify that your Firefox pkg has `sndio` support enabled:

```sh
pkg options firefox | grep SNDIO
```

* Enable and start `sndio` daemon:

```sh
service sndiod enable # this adds an /etc/rc.conf line
service sndiod start
```

* Then in Firefox go to `about:support#media` to ensure `sndio` is in fact the audio backend.

If that workaround somehow doesn't work at some point, another trick you can try is to add the following rule to Firefox's `about:config`:

```sh
media.cubeb.backend = oss
```

After setting it up, Firefox should play audio without any issues.

## Configure the network

<p id="sub-desc">FreeBSD should have set up already your network during installation. If somehow it isn't working, here are some steps to try.</p>

* Check your `/etc/rc.conf` and check if it has a line like:

```sh
ifconfig_re0="DHCP"
```

if not, add it.

* If you're using WiFi, check if your card driver is available for FreeBSD, and load its kernel driver.

## Configure brightness

<p id="sub-desc">Laptops don't have physical brightness control, instead a special set of keys in the keyboard control the bright amount. Depending on the screen you have, you can opt for some packages or some manual workaround to change brightness in your screen.</p>

The first recommendation is to add `acpi_video` into `/boot/loader.conf`. After that you can install `xbacklight` and try to get control with brightness keys in your laptop.

> If when you press the function keys nothing happens, it may be possible that your function keys need some rework with `devd()`.

Since FreeBSD 13.0, `backlight(8)` was introduced. The best way to change brightness of your laptop monitor is through this utility.

In order to use `backlight(8)` check if you have something inside the `/dev/backlight` directory.

```sh
$ ls /dev/backlight
backlight0	intel_backlight0
```

If you get something similar to the above, then you can try setting the brightness selecting the desired file:

```sh
backlight -f intel_backlight0 40
```

You can make a script to manage the value via your laptop's dedicated bright keys. There are other workarounds like changing brightness' value via `xrandr(1)` but that changes the gamma instead of physical brightness, which is not so power efficient.

## Enable Touchpad / Trackpad

<p id="sub-desc">To get the keyboard / touchpad / trackpoint working in a laptop, there are two ways: the old-fashioned way and the new way.</p>

* The old-fashioned way is using synaptics.

```sh
xf86-input-synaptics xinput
```

* The new way uses libinput instead.

```sh
libinput xf86-input-libinput
```

If you're using a Thinkpad, and you want to enable the trackpoint and use the middle button to scroll, this steps are required.

— If you're working with `synaptics`:

* In your `/boot/loader.conf` add the following line:

```sh
hw.psm.synaptics_support="1"
```

* In your `/etc/rc.conf` add the following line:

```sh
moused_enable="YES"
moused_flags="-3 -F 200 -V"
```

— If you're working with libinput:

* Remove any `moused` variable at `/etc/rc.conf`.

* Add the following line in `/etc/sysctl.conf`:

```sh
kern.evdev.rcpt_mask="12"
```

## Summing up

This is an often updated document, and this section needs to be rewritten (:

