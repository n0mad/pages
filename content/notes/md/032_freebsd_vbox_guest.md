---
title: FreeBSD in VirtualBox as a Guest
date: November 04, 2023
tgdate: 2023-11-04
shortdesc: Configure a Desktop Workspace for FreeBSD inside VirtualBox as a guest.
tgdesc: An ideal world would allow all of us to use our most beloved operating system as the one and only daily driver for work, hobby, gaming and anything in between. But let's say we don't have that ideal world yet. So what do we do? Virtualisation.
---

# FreeBSD in VirtualBox as a Guest

<figure><div class="tg-header-img"><img src="https://www.khronos.org/opengl/wiki_opengl/images/opengl_wiki.gif?6adcb" alt="OpenGL"></div></figure>

<p id="sub-desc">An ideal world would allow all of us to use our most beloved operating system as the one and only daily driver for work, hobby, gaming and anything in between.</p>

— But let's say we don't have that ideal world yet. So what do we do? Virtualisation.

> There is an extensive guide on FreeBSD virtualisation on the [Handbook](https://docs.freebsd.org/en/books/handbook/virtualization/). This guide aims to be a savvy setup only. Further reference should be taken from the Handbook entry.

## Virtualisation to the rescue

<p id="sub-desc">FreeBSD can be virtualised using [VirtualBox](https://www.virtualbox.org/). Just download an `ISO` image from the [official site](https://download.freebsd.org/releases/amd64/amd64/ISO-IMAGES/13.2/) and install it.</p>

> For this example, the host is a Windows 10 x64 machine.

FreeBSD should work out of the box on the fresh install inside VirtualBox however, in order to enhance the experience, we need to tweak it a bit.

Since we are using the OS as a guest, we should only need to install the following package:

```sh
$ doas pkg install -y virtualbox-ose-additions
```

`/etc/rc.conf`:

```sh
vboxguest_enable="YES"
vboxservice_enable="YES"
vboxservice_flags="--disable-timesync"
vboxvfs_load="YES"
```

You will notice that the moment you restart the vm, only the borderless mouse interaction works out of the box. You can enable the rest of features using the cli tool `VBoxClient`. The flag `--help` will tell you all the available options.

Some really cool feature is the bidirectional clipboard. You can enable it by typing `VBoxClient --clipboard`. If everything goes fine you should be able to perform copy-paste operations from and to both the host and guest.

> Features enabled via VBoxClient are not automatically enabled at boot time, you may need to create a custom script that enables the one you want after executing `.xinitrc`.

### Fixing the tty size

<p id="sub-desc">Once you boot FreeBSD from VirtualBox, if your screen has a big resolution you'll notice that the buffer offered by default from VirtualBox is too small.</p>

This happens due to the new virtual terminal implemented in FreeBSD, `Newcons` or `vt`. `vt` is in active development and as for now it has limited support for modesetting. It adds the following features over `sc`:

- Unicode support.
- Double-width character support for CJK characters.
- Bigger font maps (because of the use of graphics mode).
- Better use of graphics modes, enabling more appealing graphical boot experiences. 

> It's true that when using a KMS driver like `i915kms` or `radeonkms`, you can set a mode in `/boot/loader.conf` like `kern.vt.fb.default_mode="1024x768"`.

Since we are using a virtual machine with no intention on accelerated graphics (at least for now), the fastest solution to our limited tty size is to change the virtual console from `vt` to `sc`.

```sh
$ doas echo 'kern.vty=sc' >> /boot/loader.conf
```

Once the setting is changed, we can check the available modes for the `sc` tty using `vidcontrol`:

```sh
$ vidcontrol -i mode
```

> This workaround is confirmed to be working in 13.2-RELEASE. A screen of `1024x768x24` should be MODE_280

To make your selection permanent, you will have to add it to `/etc/rc.conf`: `allscreens_flags="MODE_280"`.

### Adding custom resolution(s) for X11

<p id="sub-desc">If you plan to use the virtual machine with X11, there are a couple steps you can do to improve the screen resolution once you are in the window manager of choice.</p>

First, from the VirtualBox UI under the Settings menu, select the *Display* tab. You should see there a dropdown menu for the Graphics Controller. Despite a yellow warning discouraging you to use `VBoxVGA` GC, select it.

Disable the option for Enabling 3D Acceleration if it's enabled.

Then from the OS guest, we can use `xrandr(1)` to set the resolution. If you don't find the desired resolution when executing the tool without any args, then you can try to setup a custom one.

In this guide, we are missing the `1366x768` resolution in the guest OS. We can add it by first creating a new *modeline* with:

```sh
$ cvt 1366 768 60
```

where the `60` is the refresh rate in Hz.

It should generate a line that looks like this:

```sh
Modeline "1366x768_60.00"  85.25  1366 1440 1576 1784  768 771 781 798 -hsync +vsync
```

Now, using `xrandr(1)` we can create the new mode, and add it:

```sh
$ xrandr --newmode "1366x768_60.00"  85.25  1366 1440 1576 1784  768 771 781 798 -hsync +vsync
$ xrandr --addmode "1366x768_60.00"
```

If you run `xrandr(1)` again, you should see the new mode available in the list. To make the changes permanent, add the previous two lines into `~/.xprofile`.

As a last step, we can enable the new mode from `.xinitrc`: `xrandr -s 1366x768_60.00` so each time we launch the graphic environment, the resolution is automatically set.

### Enabling shared directories

<p id="sub-desc">Using a virtual machine to work often needs that the user can share data between the host and the guest. The most common way to do so is through ssh, but we can also use shared directories from within VirtualBox.</p>

In order to achieve it this way, we need to follow these steps:

- Create a directory in your host that you want to share with the guest.
- Create a directory in your VM that will be used as a mount point.
- Add it to the VirtualBox guest properties from within the Settings UI.
- Enable `Shared Folders` in the VirtualBox Settings UI.

From the FreeBSD guest side, we can mount the shared directory as:

```sh
$ doas mount -t vboxvfs -o rw,gid=1001,uid=1001 vmshare /mnt/vmshare
```

where the `gid` and `uid` are the owner of the directory and can be obtained by running `$ id <username>`.

In order to automatically mount the shared directory on boot, we need to add the following line to `/etc/fstab`:

```sh
vmshare /mnt/vmshare vboxvfs rw,gid=1001,uid=1001 0 0
```

And from the VirtualBox host side, we have to check-enable the `Auto-mount` option at the *Settings* tab `Shared Folders -> your_shared_dir -> Edit`.

From now on, each time you boot your FreeBSD virtual machine, it will automatically mount the shared directory and you will have access to it.
