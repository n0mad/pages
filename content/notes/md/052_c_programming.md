---
title: C Programming | Working with pointers
date: January 6, 2020
tgdate: 2020-01-06
shortdesc: Learn how to use pointers in C.
tgdesc: Accessing memory locations is one of the greatest features of the C programming language, although it requires some responsibility. The word pointer often scares developers away, but it shouldn't.
tgimg: 000_freebsd_gearingup_art.png
---

# C Programming | Working with pointers

<p id="sub-desc">Accessing memory locations is one of the greatest features of the C programming language, although it requires some responsibility. The word pointer often scares developers away, but it shouldn't.</p>

> Pointers give support for dynamic memory allocation, level-up flow control in a program and are closer to hardware which makes code more efficient.

## What are pointers?

<p id="sub-desc">In C programming, variables hold values at a specific memory address. Pointers are variables that hold memory addresses and types of other variables and functions, giving direct access to physical memory locations anywhere inside the computer.</p>

> Given the variable var, &var is a pointer to var.

You can think about pointers and variables like license-plates and vehicles. While vehicles can seize too many types and forms, license-plates usually come in an unified form.

## How pointers work

<p id="sub-desc">With pointers it's possible to access any memory location and change the data contained at that location.</p>

A pointer is declared by adding an asterisk (\*) in front of the variable name in the declaration statement.

> It's heavily recommended to initialize pointers as `NULL` since when we create a pointer it isn't initialized and holds random data that can point to anywhere in the computer memory.

```c
int variable_name = 8;         //define a variable
int *variable_pointer = NULL;  //define a pointer to a variable
```

`NULL` is a macro to address `0`. In programming terms 0 is an invalid address. It can be defined like this:

```
#define NULL ((void*)0)
```

— In order to work with declared pointers, we have two basic pointer operators:

 * `&` Address constant of an object in memory. Given a variable, point to it.

```c
/* we pass a variable asking for its memory address */
printf("%d\n", &variable_name);

/* the program should return a memory address */
"0xfbee324b"
```

* `*` Content of a memory address. Given a pointer, get the value stored in it. This is usually called pointer dereferencing.

```c
/* make our pointer point to the address of the given variable */
variable_pointer = &variable_name;

/* we pass a variable asking for its value */
printf("%d\n", *variable_pointer);

/* the program should return the content of the memory address */
"8"
```

Let's make a quick reminder of how to work with simple pointers:

```c
int main() {
/* define a variable for a number */
  int num;
  
/* define a pointer to num */
  int *int_ptr = NULL;

/* add a value to num */
  num = 14;

/* now make the pointer point to num. This assigns num address to int_ptr */
  int_ptr = &num;

/* let's check what values contain each variable */
  printf("num      = %d\n", num);
  printf("&num     = %p\n", &num);
  printf("int_ptr  = %p\n", int_ptr);
  printf("*int_ptr = %p\n", *int_ptr);
  printf("&int_ptr = %p\n", &int_ptr);

/* int_ptr points to num, changing int_ptr value modifies num too */
  *int_ptr = 8;
  printf("modified *int_ptr = %p modified num to num = %d\n", *int_ptr, num);

  return 0;
}
```

The result of that program should be similar to this:

```sh
num      = 14
&num     = 0x6fff86d087a5
int_ptr  = 0x6fff86d087a5
*int_ptr = 14
&int_ptr = 0x6fff86d087a5
modified *int_ptr = 8 modified num to num = 8
```

## Pointer utilities

<p id="sub-desc">We've seen a quick refresh of how pointers work. Now let's take a look at some options pointers give to us.</p>

— We can have multiple pointers pointing to the same variable.

```c
int main() {
  int num;
  int *first_ptr;
  int *second_ptr;
  
  num = 14;
  
  first_ptr = &num;
  second_ptr = first_ptr;

  return 0;
}
```

Since `first_ptr` and `second_ptr` are both pointers we can reference them.

— We can pass pointers as function arguments.

Passing data using a pointer allows the function to modify the external data.

If we try to do the same with data passed as values instead of pointers then we only modify the function parameter, and not the original value since the addresses of the parameter and the variable in main are not the same.

```c
void modify_data(int *data);

int main() {
	int externalData = 10; 

	printf("External data value before modify is %d\n", externalData);

	ModifyData(&externalData);

	printf("External data value after been modified is %d\n", externalData);
}

void modify_data(int *data) {
	*data = 0;
}
```

The result should be:

```sh
External data value before modify is 10 
External data value after been modified is 0
```

However, we can't change the actual pointer to the data since passed a copy of the pointer.

A common practical example using pointers as function parameters is a swap function.

```c
void swap_float( float *a, float *b) {
	float tmp = *a; 
	*a = *b; 
	*b = tmp; 
}
```

— We can pass pointer to a pointer as a function argument.

This way we can modify the original pointer and not its copy. Similar to passing a variable in the previous example.

— We can return pointers.

This is pretty much straight forward. We have to declare the return type to be a pointer to the appropriate data type.

```c
int *round_float(float num);
```

An example implementing the function:

```c
int *round_float(float *num);

int main() {
	float fnum = 5.23;
	int *frounded;

	frounded = round_float(&fnum);

	printf("rounded value from %f is %d.", fnum, *frounded);
	return 0;
}

int *round_float(float *num) {
	int *tmp = ((*num + 0.5f) *1) /1;

	return tmp;
}
```

— We can create function pointers.

A function pointer is a variable that stores the address of a function to be used later on the program.

```c
typedef float (*operations_table)(float, float);
```

When we call a function, we might need to pass the data for it to process along pointers to subroutines that determine how it processes the data.

```c
typedef float (*operations_table)(float, float);

float add( float x, float y) { 
	return x+y;
}
float sub( float x, float y) { 
	return x-y;
}

float operate(operations_table op_table, float x, float y) { 
	return op_table(x,y);
};

int main() {
	int a, b;
	int a = 5;
	int b = 10;

	operate(add, a, b);
}
```

Another option is to store function pointers in arrays and later call the functions using the array index notation.

```c
float add( float x, float y) { 
	return x+y;
}
float sub( float x, float y) { 
	return x-y;
}

float (*operations_table[2])(float, float) = { add, sub};

int main() {
	int a, b;
	int a = 5;
	int b = 10;

	operations_table[0](a, b);
}
```

— We can use pointers with `structs`.

Normally we access `struct` components with a dot `.` but when a `struct` is marked as pointer, we access their values using the point-to operator `->`.

> Note that is possible to still use a dot `.` , but then the call to the component is as follows: `(*foo).variable`.

```c
typedef struct {
	int x, y, z;
} vector3_t;

int main() {
	vector3_t origin = {3, 5, 10};

	vector3_t *point;

	point->x = origin.x;
	point->y = 0;
	point->z = origin.z;

	printf("\npoint values are x = %d | y = %d | z = %d", (*origin).x, (*origin).y, (*origin).z);
} 
```

— We can define strings.

There's no such thing recognized as a string in C. Strings in C are arrays of characters terminated with a `NUL` (represented as `\0`).

```c
char *title = "unixworks";
```

The array way of creating a string literal would be:

```c
char title[] = "unixworks";
```

Which is the equivalent to:

```c
char title[] = {'u', 'n', 'i', 'x', 'w', 'o', 'r', 'k', 's', '\0'};
```

Note that using the pointer approach to create strings doesn't allow to modify the string later as it's supposed to be treated as a `const`.

However, we can work with the string pointer as an array. It will return the value of the first character, since the variable actually points exactly to the beginning of the string.

## Pointers and arrays

<p id="sub-desc">Although pointers and arrays aren't the same thing, they can work hand to hand in C. In most of the cases, the name of the array is converted to a pointer to the first element.</p>

* An array notation like `array[index]` can be achieved using pointers with `*(array + index)`.
* The same way the array notation `&array[index]` can be achieved using pointer notation `array + index`.

Arrays in C programming need to have its size declared when we create them, or at least we are told to do so when learning. Other programming languages can perform dynamic arrays without declaring its size when created.

—The fact is that we can create dynamic arrays in C combining pointers and arrays. Managing memory in real-time is extremely useful to arrays that are generated at run-time.

The only prerequisite to create a dynamic array using pointers is to reserve memory for it. That is achieved calling `malloc()`.

```c
int *num_ptr;
num_ptr = malloc(MAX_NUMBERS * sizeof(int));
```

Where:

* `(int *)` casts the data type.
* `MAX_NUMBERS` can be whatever value that determines the max elements in the array.
* `sizeof(int)` is the amount of bytes that each element in the array holds.

## Dynamic array of void pointers

<p id="sub-desc">A useful utility mixing pointers and arrays we can create is a dynamic array of void pointers.</p>

In order to demonstrate this, we will start defining a `struct` as follows:

```c
typedef struct {
	void **data;
	int capacity;
	int count;
} * set_t;
```

Where we have:

* `void **data` that are `void` pointers stored as a dynamic arrays. When used in a pointer, void defines a generic pointer (pointer of any type).
* `capacity` which is the total allowed items.
* `count` which is the current amount of items. It acts as an index for the stored data.

We can initialize our `set_t` structure with the same criteria for dynamic arrays, using `malloc()`:

```c
set_t set = malloc(sizeof(struct set_t)); 

*set = (struct set_t) { 
	.count = 0, 
	.capacity = 1, 
	.data = malloc(sizeof(void *))
};
```

If we want to add data to our list, we can increase the count value of our `struct`:

```c
set->count += 1;
```

and compare it against the capacity value.

```c
if(set->count == set->capacity) {
	set->capacity *=2; 
	set->data = realloc(set->data, set->capacity * sizeof(void *));
}
```

As most of the data structures, this dynamic array of pointers becomes useful when we create some functions to work with it.

As an example we can make a function to get a value from an index of the set, and another one to check if a value is contained in an index of the set.

```c
void *index_value(set_t set, int index) { 
	if (index > set->count) {
		printf("Index is out of bounds.\n");
		exit(1);
	}
	return set->data[index];
}

void set_contains(set_t set, void * value) {
	for (int i = 0; i < set->count; i++) { 
		if (Index(set, i) == value) 
			printf("Value is in the set.\n"); 
	}
	printf("Value is not in the set.\n");
}
```

## Linked lists

<p id="sub-desc">Arrays are fine, but they can be inefficient depending the program to create and the target device architecture.</p>

Linked lists are a data structure. Instead of asking a large contiguous block of memory in a request to store an array, ask for one data unit at a time, for one element at a time in a separate request.

Let's say we have some data we want to store as a list.

```c
int x, y, z, w;
```

This makes the memory to allocate the data in non contiguous memory blocks. But we need to link the memory blocks in some way.

```c
+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
|  x  |     |     |  y  |     |  z  |     |     |     |  w  |     |     |
+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
```

One common solution is to store next to each data value, the memory address of the next data block.

—This can be represented in C creating a `struct`, which we can name `node`, where we store the data value, and the next node address (a pointer):

```c
typedef struct node {
	int data;
	node* next;
} node_t;
```

This way we'll have something like this:

```c
+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
|  x  |y_mem|     |  y  |z_mem|  z  |w_mem|     |  w  |  0  |     |     |
+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
```

instead of

```
arr[3] = {x, y, z, w};

+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
|     | a[0]| a[1]| a[2]| a[3]|     |     |     |     |     |     |     |
+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+
```

We are using some extra memory in the list compared with the array method, but that gives us the ability to create and free nodes dynamically where we need, and when we need.

—Worth mention here is the last node of the list points to NULL or 0 as the next node address, indicating there's no more data in the list.

In the other hand, the address of the first node of the list gives us access to the complete linked list. Usually this first node is called head.

```c
node_t *head = NULL;
```

If we'd need to create another data link to our list, we first need to create a separate node, and then link the last node address to the newly created node instead of `NULL`.

```c
node_t *node_a; 

node_a = malloc(sizeof(node_t));
node_a->data = 10;

head = node_a;

We can also insert nodes anywhere in the list. The only thing to take care is to relocate the address values of each node.

node_t *head = NULL;
node_t *node_a, *node_b; 

node_a = malloc(sizeof(node_t));
node_b = malloc(sizeof(node_t));

node_a->data = 10;
node_b->data = 43;

head = node_b;

node_b->next = node_a;
node_a->next = NULL;
```

At this point the process starts to repeat itself a lot, and programming is intended to automate tasks. We can organize this a bit, creating a function that creates nodes for us.

```c
node_t *create_node(int data) { 
	node_t *result = malloc(sizeof(node_t));
	result->data = data;
	result->next = NULL;
	return result;
}
```

This way we can start working dynamically and add nodes each time we need them.

```c
node_t *head = NULL;
node_t *dummy; 

dummy = create_node(10);
head = dummy;

dummy = create_node(43);
dummy->next = head;
head = dummy;
```

This data structures are more useful if we implement functions to work with them. As an example, we can make a function to locate a node inside the list.

```c
node_t *locate_node(node_t *head, int data) {
	node_t *tmp = head;
	while(tmp != NULL) {
		if (tmp->data == data)
			return tmp;
		tmp = tmp->next;
	}
	return NULL;
}
```

Another great feature in linked lists is the possibility to insert nodes at a certain point of the list. We can make a function for it too:

```c
void insert_node_at(node_t *insert_point, node_t *new_node) {
	new_node->next = insert_point->next;
	insert_point->next = new_node;
}
```

## Summing up

<p id="sub-desc">Pointers open a huge field of possibilities in C but remember, with great power comes great responsibility.</p>

We have to take care of the heap use in some way. Using pointers introduce us the power to dynamically allocate elements in memory and this can cause to out of memory errors.

When using `malloc(3)` to allocate memory, we know that it will return `NULL` if it runs out of memory, so a good practice is to check if we really got the memory needed when allocating.

```c
pointer_var = malloc(sizeof(type_t)); 

if (pointer_var == NULL) {
	printf("out of memory\n");
	exit(1);
}
```

Another good practice is to free up memory once we are done using it. A reminder on working with memory can be found here.
