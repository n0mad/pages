<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<html lang="en-US">
	<title>C Programming | Working with headers</title>

	<meta name="MobileOptimized" content="320">
	<meta name="referrer" content="no-referrer">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="description" content="Learn how to use header files in C.">
	<meta name="keywords" content="freeBSD, unix, unixworks, c programming, opengl, vulkan, glsl, blender, 3d, substance, workshop, tutorial, linux" />
	<meta property="og:site_name" content="vertexfarm">
	<meta property="og:description" content="Eventually at a certain point in our development, we'll have a big single source file or we'll need to reuse code from a source file in another source. Header files are just source C files with a .h extension that have C code inside.">
	<meta property="og:image" content="https://codeberg.org/n0mad/pages/raw/branch/master/content/assets/img/000_freebsd_gearingup_art.png">
	<meta property="article:published_time" content="2020-01-06">
	<meta name="telegram:channel" content="@vertexfarm">
	<link rel="canonical" href="https://vertexfarm.xyz">
	<link rel="stylesheet" href="../../styles/styles.css">
	<link rel="stylesheet" href="../../styles/tango.css">
	<link href="../../icons/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<!-- Pixel Code for https://overtracking.com/ -->
	<script defer src="https://cdn.overtracking.com/t/t0mTWmkp4QlhdGi4b/"></script>
	<!-- END Pixel Code -->
	<!-- buymeacoffee.com widget -->
	<script data-name="BMC-Widget" data-cfasync="false" src="https://cdnjs.buymeacoffee.com/1.0.0/widget.prod.min.js" data-id="n0madcoder" data-description="Support me on Buy me a coffee!" 
	data-message="" 
	data-color="#9faac6" data-position="Right" data-x_margin="18" data-y_margin="18"></script>
	<!-- END buymeacoffee.com widget -->
	<script src="../../js/clipboard.min.js"></script>
	<script>
		window.onload = function() {
			let pre = document.getElementsByTagName('pre');
	
			for (let i = 0; i < pre.length; i++) {
				let b = document.createElement('button');
				b.className = 'clipboard';
				b.textContent = ' ';
				if (pre[i].childNodes.length === 1 && pre[i].childNodes[0].nodeType === 3) {
					let div = document.createElement('div');
					div.textContent = pre[i].textContent;
					pre[i].textContent = '';
					pre[i].appendChild(div);
				}
				pre[i].appendChild(b);
			}
	
			let clipboard = new ClipboardJS('.clipboard', {
			   target: function(b) {
					let p = b.parentNode;
					if (p.className.includes("highlight")) {
						let elems = p.getElementsByTagName("code");
						if (elems.length > 0)
							return elems[0];
					}
					return p.childNodes[0];
				}
			});
	
			clipboard.on('success', function(e) {
				e.clearSelection();
			});
	
			clipboard.on('error', function(e) {
				console.error('Action:', e.action, e);
				console.error('Trigger:', e.trigger);
			});
		};
	</script>
</head>

<body>
	<div class="nav">
		<h1><a href="../../index.html">vertex farm</a></h1>
		<h2>n0mad coder's blog</h2>
		<ul>
			<li><a href="../000_about.html">About</a></li>
			<li><a href="../notes/000_index.html">Notes</a></li>
			<li><a href="https://codeberg.org/n0mad">Code</a></li>
			<li><a href="https://n0madcoder.itch.io/">Art</a></li>
		</ul>
	</div>

	<div class="content">
<h1 id="c-programming-working-with-headers">C Programming | Working with headers</h1>
<figure>
	<div class="tg-h-img">
	<img src="../assets/img/000_freebsd_gearingup_art.png" alt="FreeBSD gearing-up">
	</div>
</figure>
<div class="toc-wrapper"><h2>Index</h2><ul><li><a href="#why-we-need-header-files">Why we need header files</a></li><li><a href="#header-guards">Header guards</a></li><li><a href="#compiler-guards">Compiler guards</a></li><li><a href="#other-types-to-include">Other types to include</a></li><li><a href="#summing-up">Summing up</a></li></ul><hr></div>
<p id="sub-desc">
Eventually at a certain point in our development, we'll have a big single source file or we'll need to reuse code from a source file in another source. Header files are just source C files with a .h extension that have C code inside. They are designed to store function declarations and macros.
</p>
<blockquote>
<p>Header files are not mandatory but play a big game when sharing code between source files. They also help creating documentation and make code cleaner and more tidy.</p>
</blockquote>
<h2 id="why-we-need-header-files">Why we need header files</h2>
<p id="sub-desc">
The way C is designed, requires the programmer to declare what functions he's going to use before defining them. This means that the compiler needs to know that there is some function Foo that takes parameters x and y before taking care of what Foo does inside.
</p>
<p>There are two types of C header files:</p>
<ul>
<li>Built-in header files.</li>
<li>Programmer-defined header files.</li>
</ul>
<p>The first ones are provided by the C standard library, the GNU C library and similar.</p>
<p>The user-defined header files are the ones that we need to create manually and fill with our content.</p>
<p>— If we create a function <code>calcradius</code> that takes one float value for a circle's circumference and return the result, we have to declare it first:</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode c"><code class="sourceCode c"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true" tabindex="-1"></a><span class="co">//function declaration</span></span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true" tabindex="-1"></a><span class="dt">float</span> calcradius <span class="op">(</span><span class="dt">float</span> circumference<span class="op">);</span></span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true" tabindex="-1"></a><span class="co">//function definition</span></span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true" tabindex="-1"></a><span class="dt">float</span> calcradius <span class="op">(</span><span class="dt">float</span> circumference<span class="op">)</span> <span class="op">{</span></span>
<span id="cb1-6"><a href="#cb1-6" aria-hidden="true" tabindex="-1"></a>    <span class="dt">float</span> result<span class="op">;</span></span>
<span id="cb1-7"><a href="#cb1-7" aria-hidden="true" tabindex="-1"></a>    result <span class="op">=</span> circumference <span class="op">/</span> <span class="dv">2</span> <span class="op">*</span> <span class="fl">3.14</span><span class="bu">f</span><span class="op">;</span></span>
<span id="cb1-8"><a href="#cb1-8" aria-hidden="true" tabindex="-1"></a>    <span class="cf">return</span> result<span class="op">;</span></span>
<span id="cb1-9"><a href="#cb1-9" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span></code></pre></div>
<p>The function at the beginning is a declaration. It exists somewhere in the program but there's no memory allocated to it.</p>
<p>The detailed function below is the function definition.</p>
<p>— Header files describe what you can use from the outside module while the function definitions are stored in a source file with a .c extension.</p>
<p>When the compiler runs, it copies and pastes each header file included in a source file at the beginning of the code.</p>
<p>To implement a header in our calculation program we need to create two files, <code>math.h</code> and <code>math.c</code>.</p>
<p>Now we can cut and paste our function declaration inside math.h:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode c"><code class="sourceCode c"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true" tabindex="-1"></a>math<span class="op">.</span>h</span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true" tabindex="-1"></a><span class="co">//function declaration</span></span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true" tabindex="-1"></a><span class="dt">float</span> calcradius <span class="op">(</span><span class="dt">float</span> circumference<span class="op">);</span></span></code></pre></div>
<p>In order to link them together, inside the math.c file we need to add the math.h file at the beginning of the program, using the #include directive:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode c"><code class="sourceCode c"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true" tabindex="-1"></a>math<span class="op">.</span>c</span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true" tabindex="-1"></a><span class="pp">#include </span><span class="im">&quot;math.h&quot;</span></span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb3-5"><a href="#cb3-5" aria-hidden="true" tabindex="-1"></a><span class="co">//function definition</span></span>
<span id="cb3-6"><a href="#cb3-6" aria-hidden="true" tabindex="-1"></a><span class="dt">float</span> calcradius <span class="op">(</span><span class="dt">float</span> circumference<span class="op">)</span> <span class="op">{</span></span>
<span id="cb3-7"><a href="#cb3-7" aria-hidden="true" tabindex="-1"></a>    <span class="dt">float</span> result<span class="op">;</span></span>
<span id="cb3-8"><a href="#cb3-8" aria-hidden="true" tabindex="-1"></a>    result <span class="op">=</span> circumference <span class="op">/</span> <span class="dv">2</span> <span class="op">*</span> <span class="fl">3.14</span><span class="bu">f</span><span class="op">;</span></span>
<span id="cb3-9"><a href="#cb3-9" aria-hidden="true" tabindex="-1"></a>    <span class="cf">return</span> result<span class="op">;</span></span>
<span id="cb3-10"><a href="#cb3-10" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span></code></pre></div>
<p>This should be enough for the <code>.c</code> source file however, inside our <code>.h</code> file we have to perform some extra work in order to prevent some future errors that can happen when our program grows.</p>
<h2 id="header-guards">Header guards</h2>
<p id="sub-desc">
<p>We know when the compiler runs, it's going to copy-paste our header file in each <code>#include directive</code>. Since we can include the same header file in multiple <code>.c</code> source files we need a way to not copy-paste the same header multiple times.</p>
<p>The C programming language provides the <code>#ifndef</code> and <code>#define</code> directives to help with this problem.</p>
<p>— <code>#ifdef</code>'s are pre-processor directives that are here needed to ensure the header file is only included once. Otherwise we would face an error similar to this when we run the compiler:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode sh"><code class="sourceCode bash"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true" tabindex="-1"></a><span class="ex">./math.h:4:7</span> error redefinition of calcradius function</span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true" tabindex="-1"></a><span class="ex">./math.h:4:7</span> note previous definition is here</span></code></pre></div>
<p>They work similar to <code>if</code> statements. The best way to protect our header for duplication is to name it with a unique identifier (usually its file name) and check if it's already defined or not. If not, then it's copied until the end of the condition.</p>
<p>Let's guard our header file:</p>
<div class="sourceCode" id="cb5"><pre class="sourceCode c"><code class="sourceCode c"><span id="cb5-1"><a href="#cb5-1" aria-hidden="true" tabindex="-1"></a>math<span class="op">.</span>h</span>
<span id="cb5-2"><a href="#cb5-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-3"><a href="#cb5-3" aria-hidden="true" tabindex="-1"></a><span class="pp">#ifndef MATH_H </span><span class="co">/* This is our identifier */</span></span>
<span id="cb5-4"><a href="#cb5-4" aria-hidden="true" tabindex="-1"></a><span class="pp">#define MATH_H </span></span>
<span id="cb5-5"><a href="#cb5-5" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-6"><a href="#cb5-6" aria-hidden="true" tabindex="-1"></a><span class="co">//function declaration</span></span>
<span id="cb5-7"><a href="#cb5-7" aria-hidden="true" tabindex="-1"></a><span class="dt">float</span> calcradius <span class="op">(</span><span class="dt">float</span> circumference<span class="op">);</span></span>
<span id="cb5-8"><a href="#cb5-8" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb5-9"><a href="#cb5-9" aria-hidden="true" tabindex="-1"></a><span class="pp">#endif </span><span class="co">/* MATH_H */</span></span></code></pre></div>
<p>This way we can ensure that no matter how many times we need to include math.h in our program sources.</p>
<h2 id="compiler-guards">Compiler guards</h2>
<p id="sub-desc">
Sometimes the C source code is compiled with a C++ compiler. This can be because of part of the program has been written in C++, or we have to use some modules that are created in C++. It also can be the case where we are including some C modules inside a C++ project.
</p>
<p>— While C don't, C++ does name mangling due to function overloading. In C++ we can have two functions that have the same name with different arguments or return values, and the program can run without problems.</p>
<p><code>extern "C" {}</code> ensures the compiler to treat the code inside the <code>extern</code> as C code.</p>
<div class="sourceCode" id="cb6"><pre class="sourceCode c"><code class="sourceCode c"><span id="cb6-1"><a href="#cb6-1" aria-hidden="true" tabindex="-1"></a>math<span class="op">.</span>h</span>
<span id="cb6-2"><a href="#cb6-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb6-3"><a href="#cb6-3" aria-hidden="true" tabindex="-1"></a><span class="pp">#ifndef MATH_H </span></span>
<span id="cb6-4"><a href="#cb6-4" aria-hidden="true" tabindex="-1"></a><span class="pp">#define MATH_H </span></span>
<span id="cb6-5"><a href="#cb6-5" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb6-6"><a href="#cb6-6" aria-hidden="true" tabindex="-1"></a><span class="pp">#ifdef __cplusplus</span></span>
<span id="cb6-7"><a href="#cb6-7" aria-hidden="true" tabindex="-1"></a><span class="kw">extern</span> <span class="st">&quot;C&quot;</span> <span class="op">{</span></span>
<span id="cb6-8"><a href="#cb6-8" aria-hidden="true" tabindex="-1"></a><span class="pp">#endif</span></span>
<span id="cb6-9"><a href="#cb6-9" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb6-10"><a href="#cb6-10" aria-hidden="true" tabindex="-1"></a><span class="co">//function declaration</span></span>
<span id="cb6-11"><a href="#cb6-11" aria-hidden="true" tabindex="-1"></a><span class="dt">float</span> calcradius <span class="op">(</span><span class="dt">float</span> circumference<span class="op">);</span></span>
<span id="cb6-12"><a href="#cb6-12" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb6-13"><a href="#cb6-13" aria-hidden="true" tabindex="-1"></a><span class="pp">#ifdef __cplusplus</span></span>
<span id="cb6-14"><a href="#cb6-14" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span>
<span id="cb6-15"><a href="#cb6-15" aria-hidden="true" tabindex="-1"></a><span class="pp">#endif</span></span>
<span id="cb6-16"><a href="#cb6-16" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb6-17"><a href="#cb6-17" aria-hidden="true" tabindex="-1"></a><span class="pp">#endif </span><span class="co">/* MATH_H */</span></span></code></pre></div>
<p>Now we can work with the function <code>calcradius</code> in any other <code>.c</code> source file without worrying when our program grows.</p>
<h2 id="other-types-to-include">Other types to include</h2>
<p id="sub-desc">
We've seen how to declare functions in header files and define them later in source files. In header files we can also include <code>structs</code> and variables without giving them any values. In the .c source file we can access those variables and initialize them.
</p>
<p>If we initialize variables with values in header files, the compiler is going to prompt an error when it runs.</p>
<div class="sourceCode" id="cb7"><pre class="sourceCode c"><code class="sourceCode c"><span id="cb7-1"><a href="#cb7-1" aria-hidden="true" tabindex="-1"></a>math<span class="op">.</span>h</span>
<span id="cb7-2"><a href="#cb7-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb7-3"><a href="#cb7-3" aria-hidden="true" tabindex="-1"></a><span class="op">...</span></span>
<span id="cb7-4"><a href="#cb7-4" aria-hidden="true" tabindex="-1"></a><span class="dt">float</span> givenCircumference<span class="op">;</span></span>
<span id="cb7-5"><a href="#cb7-5" aria-hidden="true" tabindex="-1"></a><span class="op">...</span></span></code></pre></div>
<p>— There's an exception with declaring values in header files, which are constants. We know (in the example above) that <code>3.14~</code> is the <code>PI</code> value and that value never changes, it's a constant.</p>
<p>Let's add it into our header by defining the value:</p>
<div class="sourceCode" id="cb8"><pre class="sourceCode c"><code class="sourceCode c"><span id="cb8-1"><a href="#cb8-1" aria-hidden="true" tabindex="-1"></a>math<span class="op">.</span>h</span>
<span id="cb8-2"><a href="#cb8-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb8-3"><a href="#cb8-3" aria-hidden="true" tabindex="-1"></a><span class="pp">#define PI 3.14159f</span></span>
<span id="cb8-4"><a href="#cb8-4" aria-hidden="true" tabindex="-1"></a><span class="op">...</span></span>
<span id="cb8-5"><a href="#cb8-5" aria-hidden="true" tabindex="-1"></a><span class="dt">float</span> givenCircumference<span class="op">;</span></span>
<span id="cb8-6"><a href="#cb8-6" aria-hidden="true" tabindex="-1"></a><span class="op">...</span></span></code></pre></div>
<div class="sourceCode" id="cb9"><pre class="sourceCode c"><code class="sourceCode c"><span id="cb9-1"><a href="#cb9-1" aria-hidden="true" tabindex="-1"></a>math<span class="op">.</span>c</span>
<span id="cb9-2"><a href="#cb9-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb9-3"><a href="#cb9-3" aria-hidden="true" tabindex="-1"></a><span class="op">...</span></span>
<span id="cb9-4"><a href="#cb9-4" aria-hidden="true" tabindex="-1"></a>result <span class="op">=</span> circumference <span class="op">/</span> <span class="dv">2</span> <span class="op">*</span> PI<span class="op">;</span></span>
<span id="cb9-5"><a href="#cb9-5" aria-hidden="true" tabindex="-1"></a><span class="op">...</span></span></code></pre></div>
<p>Our final files should look like this:</p>
<div class="sourceCode" id="cb10"><pre class="sourceCode c"><code class="sourceCode c"><span id="cb10-1"><a href="#cb10-1" aria-hidden="true" tabindex="-1"></a>math<span class="op">.</span>h</span>
<span id="cb10-2"><a href="#cb10-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb10-3"><a href="#cb10-3" aria-hidden="true" tabindex="-1"></a><span class="pp">#ifndef MATH_H </span><span class="co">/* This is our identifier */</span></span>
<span id="cb10-4"><a href="#cb10-4" aria-hidden="true" tabindex="-1"></a><span class="pp">#define MATH_H </span></span>
<span id="cb10-5"><a href="#cb10-5" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb10-6"><a href="#cb10-6" aria-hidden="true" tabindex="-1"></a><span class="pp">#define PI 3.14159f</span></span>
<span id="cb10-7"><a href="#cb10-7" aria-hidden="true" tabindex="-1"></a><span class="dt">float</span> radius<span class="op">;</span></span>
<span id="cb10-8"><a href="#cb10-8" aria-hidden="true" tabindex="-1"></a><span class="dt">float</span> circumference<span class="op">;</span></span>
<span id="cb10-9"><a href="#cb10-9" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb10-10"><a href="#cb10-10" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb10-11"><a href="#cb10-11" aria-hidden="true" tabindex="-1"></a><span class="pp">#ifdef __cplusplus</span></span>
<span id="cb10-12"><a href="#cb10-12" aria-hidden="true" tabindex="-1"></a><span class="kw">extern</span> <span class="st">&quot;C&quot;</span> <span class="op">{</span></span>
<span id="cb10-13"><a href="#cb10-13" aria-hidden="true" tabindex="-1"></a><span class="pp">#endif</span></span>
<span id="cb10-14"><a href="#cb10-14" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb10-15"><a href="#cb10-15" aria-hidden="true" tabindex="-1"></a><span class="co">//function declaration</span></span>
<span id="cb10-16"><a href="#cb10-16" aria-hidden="true" tabindex="-1"></a><span class="dt">float</span> calcradius <span class="op">(</span><span class="dt">float</span> circumference<span class="op">);</span></span>
<span id="cb10-17"><a href="#cb10-17" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb10-18"><a href="#cb10-18" aria-hidden="true" tabindex="-1"></a><span class="pp">#ifdef __cplusplus</span></span>
<span id="cb10-19"><a href="#cb10-19" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span>
<span id="cb10-20"><a href="#cb10-20" aria-hidden="true" tabindex="-1"></a><span class="pp">#endif</span></span>
<span id="cb10-21"><a href="#cb10-21" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb10-22"><a href="#cb10-22" aria-hidden="true" tabindex="-1"></a><span class="pp">#endif </span><span class="co">/* MATH_H */</span></span>
<span id="cb10-23"><a href="#cb10-23" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb10-24"><a href="#cb10-24" aria-hidden="true" tabindex="-1"></a>math<span class="op">.</span>c</span>
<span id="cb10-25"><a href="#cb10-25" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb10-26"><a href="#cb10-26" aria-hidden="true" tabindex="-1"></a><span class="pp">#include </span><span class="im">&quot;math.h&quot;</span></span>
<span id="cb10-27"><a href="#cb10-27" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb10-28"><a href="#cb10-28" aria-hidden="true" tabindex="-1"></a><span class="co">//function definition</span></span>
<span id="cb10-29"><a href="#cb10-29" aria-hidden="true" tabindex="-1"></a><span class="dt">float</span> calcradius <span class="op">(</span><span class="dt">float</span> circumference<span class="op">)</span> <span class="op">{</span></span>
<span id="cb10-30"><a href="#cb10-30" aria-hidden="true" tabindex="-1"></a>    <span class="dt">float</span> result<span class="op">;</span></span>
<span id="cb10-31"><a href="#cb10-31" aria-hidden="true" tabindex="-1"></a>    result <span class="op">=</span> circumference <span class="op">/</span> <span class="dv">2</span> <span class="op">*</span> PI<span class="op">;</span></span>
<span id="cb10-32"><a href="#cb10-32" aria-hidden="true" tabindex="-1"></a>    <span class="cf">return</span> result<span class="op">;</span></span>
<span id="cb10-33"><a href="#cb10-33" aria-hidden="true" tabindex="-1"></a><span class="op">}</span></span></code></pre></div>
<h2 id="summing-up">Summing up</h2>
<p id="sub-desc">
We can create a <code>main.c</code> file to perform the needed operations, and by including the <code>math.h</code> header, we should be able to access any function or value stored inside it, since the compiler is going to know where to find those values and functions when it runs.
</p>
<div class="sourceCode" id="cb11"><pre class="sourceCode c"><code class="sourceCode c"><span id="cb11-1"><a href="#cb11-1" aria-hidden="true" tabindex="-1"></a>main<span class="op">.</span>c</span>
<span id="cb11-2"><a href="#cb11-2" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb11-3"><a href="#cb11-3" aria-hidden="true" tabindex="-1"></a><span class="pp">#include </span><span class="im">&lt;stdio.h&gt;</span></span>
<span id="cb11-4"><a href="#cb11-4" aria-hidden="true" tabindex="-1"></a><span class="pp">#include </span><span class="im">&quot;math.h&quot;</span></span>
<span id="cb11-5"><a href="#cb11-5" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb11-6"><a href="#cb11-6" aria-hidden="true" tabindex="-1"></a><span class="dt">int</span> main<span class="op">()</span> <span class="op">{</span></span>
<span id="cb11-7"><a href="#cb11-7" aria-hidden="true" tabindex="-1"></a>    <span class="co">/* assign a value for our declared float */</span></span>
<span id="cb11-8"><a href="#cb11-8" aria-hidden="true" tabindex="-1"></a>    given_circumference <span class="op">=</span> <span class="fl">4.8</span><span class="bu">f</span><span class="op">;</span></span>
<span id="cb11-9"><a href="#cb11-9" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb11-10"><a href="#cb11-10" aria-hidden="true" tabindex="-1"></a>    <span class="co">/* execute our function */</span></span>
<span id="cb11-11"><a href="#cb11-11" aria-hidden="true" tabindex="-1"></a>    printf<span class="op">(</span><span class="st">&quot;Radius from circle is: %.2f </span><span class="sc">\n</span><span class="st">&quot;</span><span class="op">,</span> calcradius<span class="op">(</span>givenCircumference<span class="op">));</span></span>
<span id="cb11-12"><a href="#cb11-12" aria-hidden="true" tabindex="-1"></a></span>
<span id="cb11-13"><a href="#cb11-13" aria-hidden="true" tabindex="-1"></a>    <span class="cf">return</span> <span class="dv">0</span><span class="op">;</span></span>
<span id="cb11-14"><a href="#cb11-14" aria-hidden="true" tabindex="-1"></a>    <span class="op">}</span></span></code></pre></div>
<p>Now you can start growing up your own math library and use it in every project that needs one (:</p>
	</div>
	<footer>
		<div class="footer-grid">
			<div class="footer-info">
			<p>2025 - n0madcoder<br>
			<a href="mailto:nomadcoder@vertexfarm.xyz">nomadcoder@vertexfarm.xyz</a></p>
			</div>
			<div class="footer-social">
				<a href="https://t.me/s/vertexfarm">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M12 22C6.477 22 2 17.523 2 12S6.477 2 12 2s10 4.477 10 10-4.477 10-10 10zm-3.11-8.83l.013-.007.87 2.87c.112.311.266.367.453.341.188-.025.287-.126.41-.244l1.188-1.148 2.55 1.888c.466.257.801.124.917-.432l1.657-7.822c.183-.728-.137-1.02-.702-.788l-9.733 3.76c-.664.266-.66.638-.12.803l2.497.78z"/></svg>
				</a>
				<a href="https://buymeacoffee.com/n0madcoder">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24"><path fill="none" d="M0 0h24v24H0z"/><path d="M5 3h15a2 2 0 0 1 2 2v3a2 2 0 0 1-2 2h-2v3a4 4 0 0 1-4 4H8a4 4 0 0 1-4-4V4a1 1 0 0 1 1-1zm13 2v3h2V5h-2zM2 19h18v2H2v-2z"/></svg>
				</a>
			</div>
		</div>
	</footer>
</body>

</html>
