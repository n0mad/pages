---
title: Chromebook | From ChromeOS to FreeBSD
date: December 16, 2023
tgdate: 2023-12-16
shortdesc: Learn how to remove ChromeOS from a Chromebook and install FreeBSD.
tgdesc: Chromebooks are mobile devices aimed to school students or light office work. They are small, lightweight, portable, and usually have a touchscreen. But in terms of OS freedom they are a bit limited. Let's take a look at how to remove ChromeOS from a Chromebook and install FreeBSD.
tgimg: https://p1-ofp.static.pub/medias/bWFzdGVyfHJvb3R8MTQ2ODk0fGltYWdlL3BuZ3xoODkvaDNmLzEwODY1MjA4MDk4ODQ2LnBuZ3xmNjJlYzMwZmMyNGFiOWNiMDkyYjMxYjMxY2RlNDY4M2Y4ZWI3ZjAyMGYwNGVjMGQ0Y2EzY2I0N2EzYjY3MjVm/lenovo-student-chromebooks-300e-chromebooks-gen-2-gallery-1.png
---

# Chromebook | From ChromeOS to FreeBSD

<figure><div class="tg-h-img"><img src="https://p1-ofp.static.pub/medias/bWFzdGVyfHJvb3R8MTQ2ODk0fGltYWdlL3BuZ3xoODkvaDNmLzEwODY1MjA4MDk4ODQ2LnBuZ3xmNjJlYzMwZmMyNGFiOWNiMDkyYjMxYjMxY2RlNDY4M2Y4ZWI3ZjAyMGYwNGVjMGQ0Y2EzY2I0N2EzYjY3MjVm/lenovo-student-chromebooks-300e-chromebooks-gen-2-gallery-1.png" alt="Lenovo 300e 2nd Gen AMD"></div></figure>

<p id="sub-desc">Chromebooks are mobile devices aimed to school students or light office work. They are small, lightweight, portable, and usually have a touchscreen built in. But in terms of OS freedom they are a bit limited. Let's take a look at how to remove ChromeOS from a Chromebook and install FreeBSD.</p>

A few days ago I had the opporinuty to get my hands in a Chromebook device as the owners were no longer interested on it and decided to take it away. The model in question is a [Lenovo 300e Chromebook 2nd Gen](https://www.lenovo.com/us/en/p/laptops/lenovo/lenovo-edu-chromebooks/lenovo-300e-chromebook-2nd-gen-ast/82ce001lus) that comes with a 64bit AMD APU.

After inspecting the hardware and turning the OS to check its behavior, I noticed that the ChromeOS not only was slow and tedious to work with but it also had limited functionality available. I wasn't able to unlock the so called "developer mode" for the Chromebook, nor enable the "Linux inside ChromeOS" feature since according to the OS itself, those features were not supported. So appart from browsing the web and using Google Drive, there was little to do with the Chromebook.

Nevertheless, the hardware was interesting enough even by using a low tier AMD APU processor, the only thing that I saw I could change to make my life easier in another OS, was the WiFi card. It came with a Qualcomm Atheros 802.11ac wireless card, that I replaced with a spare Intel AC9260.

Done that, the first thing that came to my mind was to install FreeBSD on it, and test the touchscreen and the graphics card with my usual desktop configuration.

## Unlocking the device

<p id="sub-desc">Turns out that Chromebook devices tend to come factory locked so the users cannot go outside the ChromeOS boundaries. Luckly for the users, there are solutions out there.</p>

Thanks to [MrCrhomebox](https://mrchromebox.tech/) the process of unlocking a Chromebook is fairly straight forward. The Chromebook model I got seems to be supported to unlock the device. So far so good.

Manufacturers lock the devices by hardware so the key step is to remove the backlid of the device, and disconnect the battery cable from the device. After that you need to boot the device with the charging cord.

At the login screen you will have to press and hold the `esc`, `refresh` and `power` buttons in order to reboot into recovery mode. Once there you can hit `ctrl + d` to enable developer mode. Hit `enter` when asked and wait between 5 and 15 minutes to get the device unlocked. This feels more like an `Area 51` warning to force the user back than an actual unlock process but who knows.

> Transitioning to or from developer mode will wipe out all user data on the OS, so make sure you have a backup before proceeding.

Once that's done, the device should be in developer mode. Each time you boot it up it will go into a *scary* screen laying "OS verification is OFF", where you hit `ctrl + d` again and it continues to boot.

The next step is to reach a root-capable shell. On the login screen you should be able to press `ctrl + alt + f2` to enter a command line where you can login with the user `chronos`.

Close to freedom, now it's time to run the *ChromeOS Firmware Utility Script* from [MrChromebox](https://mrchromebox.tech/#fwscript).

Inside the command-line, and without being root or sudo, download and run the script by typing:

```sh
cd; curl -LOk mrchromebox.tech/firmware-util.sh && sudo bash firmware-util.sh
```

The process should launch a command-line menu, in which you should see that the Firmware WP (write protection) is already disabled. That should allow you to proceed with the instruction `Install/Update UEFI (Full ROM) Firmware`, which installs a full replacement firmware.

After going through that step, the Chromebook won't boot up on the previously installed ChromeOS, and you will have to install a new OS as your hardware is now a *regular pc*.

## Installing FreeBSD

<p id="sub-desc">Being the Chromebook a x86_64 machine, the FreeBSD installation is the same as in any other laptop/desktop you may have installed it to too. Just download a FreeBSD image, dd it to a thumbstick drive and plug it into the Chromebook.</p>

At boot time you are prompted with an option to change boot options. From there you can give priority to the USB drive and begin the OS installation.

After installing the base OS, if the system doesn't power off, or freezes at that stage execute the following line (add to `/etc/systemctl.conf` to make it a permanent change):

```sh
hw.efi.poweroff=0
```

which by default seems to come set to `true`. If `true`, sysctl uses EFI runtime services to power off in preference to ACPI.

You will have to br careful when installing the drivers for your graphics card but luckily there is a good documentation about graphics drivers in the [FreeBSD wiki](https://wiki.freebsd.org/Graphics). In the case of the 300e Chomebook, the AMD APU in use works with the `amdgpu` driver using the `stoney` firmware loaded.

As for the net drivers, the Qualcomm Atheros 802.11ac wireless card was replaced with a regular Intel one as mentioned above so there was no issue at all connecting to the internet.

Once X11 is installed and you run `startx` to your window manager or desktop environment of choice, the touchscreen works as expected. There are some projects out there like [x11-touchscreen-calibrator](https://fourdollars.github.io/x11-touchscreen-calibrator/) that can be used to calibrate your touchscreen and recognize screen rotation among other things.

Audio seems to be missing somehow. The OS detects the HDMI audio but not the internal audio card for the speakers and the jack input/output. This is something I need to further investigate.

## Summing Up

<p id="sub-desc">The 300e Chromebook is a solid compact device that you can move around without sacrificing mobility. It is a bit limited on CPU power but using it with a tailored OS installation it runs flawlessly.</p>

The battery lasts for 8+ hours in standby mode, it doesn't makes noise at all, and the touchscreen makes it fun and multipurpose to use. I'd recommend getting a matte reflection screen protection, otherwise you are looking at a mirror all the time (:

For DIY projects, as a PDF reader, for browsing the web, to connect the car diagnosis tools, core programming and similar tasks this device fits the needs, and is cheap enough to not worry much about.

