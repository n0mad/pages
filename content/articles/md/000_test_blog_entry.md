---
title: My Document
date: October 22, 2023
shortdesc: This is a short description of the document.
tgdesc: A two line length description meant for telegram IV pages.
---

# Title for Blog goes here

<figure><div><img src="https://www.khronos.org/opengl/wiki_opengl/images/opengl_wiki.gif?6adcb" alt="OpenGL"></div></figure>

The previous div can be hidden in order to generate a custom thumbnail for Telegram instant view. It can stay visible tho, since this is a blog and not a formal paper or book.

> This is supposed to be a quote on the blog

## Coding styles

<div id="sub-desc">Let's test some coding here with the blog css stylesheet.</div>

First is a single line `code` test.

<p id="pre-code">Next we have a full `c` code line (not so well programmed):</p>

```c
int main () {
	printf("Hello blog!");

	return 0;
}
```

And now we can end the coding test.
