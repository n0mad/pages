---
title: TGA File Format | Revisited
date: February 18, 2024
tgdate: 2024-02-18
shortdesc: Take a closer look at the TGA file format.
tgdesc: Out of the many image file formats that are available, TGA definitely isn't the one that dominates the market right now. But it is definitely one of the best image file formats out there, and one of the easiest to use in terms of programming.
tgimg: 000_freebsd_gearingup_art.png
---

# TGA File Format | Revisited

<p id="sub-desc">Out of the many image file formats that are available, TGA definitely isn't the one that dominates the market right now. But it is definitely one of the best image file formats out there, and one of the easiest to use in terms of programming.</p>

> TGA is a file format commonly used in the 3D graphics industry.

TGA files (short for Truevision Graphics Adapter) are uncompressed, which means that the most quality is preserved in the image, however file sizes are often much larger than image formats like PNG or JPEG.

The format stores red, green and blue channels with 1 byte (8 bits) precision (0-255) for a total of 24 bits per pixel, allowing for an optional 8-bit alpha channel that increases the total number of bits per pixel to 32. Often the color and alpha channels are used normally as color plus transparency when needed, but we can tweak the content to store up to 4 different black and white images within the same file.

## What's in the box?

<p id="sub-desc">The TGA file format stores the pixel data along with some metadata in the form of a header, and optionsally a footer, where we can store additional information about the image.<p id="sub-desc">

Looking closely under the hood we can see the following structure for the TGA file format:

- TGA header, consisting of five main fields:

| field | size | content |
| --- | --- | --- |
| 1 | (1 byte) | id size |
| 2 | (1 byte) | color map type |
| 3 | (1 byte) | image data type [^1] |
| 4 | (5 bytes) | color map origin<br>color map length<br>color map depth<br> |
| 5 | (10 bytes) | x origin<br>y origin<br>image width<br>image height<br>image pixel depth<br>image descriptor [^2] |

- TGA pixel data, which has to be parsed based on the header information about the image.

- TGA footer, which stores additional information about the image:
	- extension area offset
	- developer directory offset
	- signature
	- dot
	- null

[^1]: The header field `image data type` defines the type of data contained in the pixel data. The file format specification gives us a list of possible valid values for this field:

	| value | description |
	| --- | --- |
	| 0 | no image data included |
	| 1 | uncompressed color-mapped image |
	| 2 | uncompressed true-color (RGB) image |
	| 3 | uncompressed black and white image |
	| 9 | run-length encoded (RLE) color-mapped image |
	| 10 | run-length encoded (RLE) true-color (RGB) image |
	| 11 | run-length encoded (RLE) black and white image |
	| 32 | run-length encoded (RLE) color-mapped image, using Huffman and Delta |
	| 33 | same as 32 using a 4-pass quadtree-type process |

	Most of the time the most useful image data type is 2. If you need to work with compressed images, you can jump straight to 10.

[^2]: The header field `image descriptor` hides interesting information in separated bits:

	| bits | description |
	| --- | --- |
	| 0 - 3 | store the number of attribute bits per pixel |
	| 4 - 5 | store the image origin location |
	| 6 - 7 | should be set to 0 |

-- TODO explain RLE vs non RLE

## Using C to read and write TGA files

<p id="sub-desc">With the knowledge about the basic / minimal TGA file format structure, using C to read and write TGA files is quite easy. Most probably we only have to keep in mind the little endian format nature of TGA when setting the code.</p>

> Being stored in little endian, the tga pixel data is stored in the blue, green and red channels order.

### Preparing the playground



### Reading the TGA file


### Writing the TGA file

## Summing up
