---
title: My Document
date: October 22, 2023
shortdesc: This is a short description of the document.
tgdesc: A two line length description meant for telegram IV pages.
---

# Code Lab | Creating and using Shared libs in C

<figure><div><img src="https://www.khronos.org/opengl/wiki_opengl/images/opengl_wiki.gif?6adcb" alt="OpenGL"></div></figure>

<p id="sub-desc">This is a short description of the document.</p>

> This is supposed to be a quote on the blog



Shared libraries are .so (or in Windows .dll, or in OS X .dylib) files. All the code relating to the library is in this file, and it is referenced by programs using it at run-time. A program using a shared library only makes reference to the code that it uses in the shared library.

Static libraries are .a (or in Windows .lib) files. All the code relating to the library is in this file, and it is directly linked into the program at compile time. A program using a static library takes copies of the code that it uses from the static library and makes it part of the program. [Windows also has .lib files which are used to reference .dll files, but they act the same way as the first one].

There are advantages and disadvantages in each method:

- Shared libraries reduce the amount of code that is duplicated in each program that makes use of the library, keeping the binaries small. It also allows you to replace the shared object with one that is functionally equivalent, but may have added performance benefits without needing to recompile the program that makes use of it. Shared libraries will, however have a small additional cost for the execution of the functions as well as a run-time loading cost as all the symbols in the library need to be connected to the things they use. Additionally, shared libraries can be loaded into an application at run-time, which is the general mechanism for implementing binary plug-in systems.

- Static libraries increase the overall size of the binary, but it means that you don't need to carry along a copy of the library that is being used. As the code is connected at compile time there are not any additional run-time loading costs. The code is simply there.

Personally, I prefer shared libraries, but use static libraries when needing to ensure that the binary does not have many external dependencies that may be difficult to meet



For a static library, the code is extracted from the library by the linker and used to build the the final executable at the point you compile/build your application. The final executable has no dependencies on the library at run time

For a shared library, the compiler/linker checks that the names you link with exist in the library when the application is built, but doesn't move their code into the application. At run time, the shared library must be available.

The C programming language itself has no concept of either static or shared libraries - they are completely an implementation feature.

Personally, I much prefer to use static libraries, as it makes software distribution simpler. However, this is an opinion over which much (figurative) blood has been shed in the past.



The most significant advantage of shared libraries is that there is only one copy of code loaded in memory, no matter how many processes are using the library. For static libraries each process gets its own copy of the code. This can lead to significant memory wastage.

OTOH, a advantage of static libraries is that everything is bundled into your application. So you don't have to worry that the client will have the right library (and version) available on their system.

observed that shared library code tends to offer a cleaner code API. 

## Coding styles

<p id="sub-desc">Let's test some coding here with the blog css stylesheet.</p>

First is a single line `code` test.

<p id="pre-code">Next we have a full `c` code line (not so well programmed):</p>

```c
int main () {
	printf("Hello blog!");

	return 0;
}
```

And now we can end the coding test.

```sh
 * - &
* --- &
  * &
* --- &
 * - &
```