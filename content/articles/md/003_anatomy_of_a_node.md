---
title: GUI programming | Anatomy of a Node
date: February 03, 2024
tgdate: 2024-02-03
shortdesc: Take a closer look at the anatomy of a Node.
tgdesc: 
tgimg: https://p1-ofp.static.pub/medias/bWFzdGVyfHJvb3R8MTQ2ODk0fGltYWdlL3BuZ3xoODkvaDNmLzEwODY1MjA4MDk4ODQ2LnBuZ3xmNjJlYzMwZmMyNGFiOWNiMDkyYjMxYjMxY2RlNDY4M2Y4ZWI3ZjAyMGYwNGVjMGQ0Y2EzY2I0N2EzYjY3MjVm/lenovo-student-chromebooks-300e-chromebooks-gen-2-gallery-1.png
---

# GUI programming | Anatomy of a Node

<p id="sub-desc">